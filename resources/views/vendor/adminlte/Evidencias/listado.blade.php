@extends('adminlte::layouts.app')
@section('main-content')
    <div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-12 col-md-offset-0">
				<!--Default box -->
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title">Listado archivos</h3>
						<div class="box-tools pull-right">
							<a class="btn btn-primary btn-block" href="{{URL::to('')}}/evidencias/editar/0" type="submit" >Nueva evidenacia</a>
					{{--	<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
								<i class="fa fa-times"></i></button>
						</div>--}}
                        </div>
					</div>
					<div class="box-body">
                        <table id='tabla_usuarios' style="width:100%">
								<tr>
									<th>ID</th>
									<th>Nombre del documento</th>
									<th>Tipo de documento</th>
									<th>Fecha de entrega</th>
									<th>Editar</th>
									<th>Eliminar</th>
								</tr>
								
							
                        </table>
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->

			</div>
		</div>
	</div>
@endsection
@section('javascripts')
@parent
<script>
var tabla='tabla_usuarios';
jQuery(function($) {
    $( "#"+tabla ).html("<thead><tr><th>id</th><th>Descripción</th><th>Monto</th><th>Fecha</th><th>Estatus</th><th class='center'>Opciones</th></tr></thead><tbody></tbody>" );
    //muestratabla();
});

function muestratabla() {        
        $.ajax({
        data: "&_token={{ csrf_token() }}",
        type: "POST",
        dataType: "json",
        url: "{{URL::to('/')}}/usuarios/listado/json",
            success: function(data){
                var html = '';
                var datos=data.datos;
                if(datos.length > 0){
                    $.each(datos, function(i,item){
                        html += '<tr>'
							html += '<td>'+item.id+'</td>'
                            html += '<td>'+item.id+'</td>'
							html += '<td>'+item.id+'</td>'
                            html += '<td>'+item.id+'</td>'
							html += '<td>'+((item.estatus==1)?'Activo':'Inactivo')+'</td>'
							html += '<td class="center"><a onclick="eliminar('+item.id+');" class="waves-effect waves-light btn boton_general"><i class="material-icons">delete</i></a> <a onclick="editar('+item.id+',\''+item.descripcion+'\',\''+item.monto+'\',\''+item.fecha+'\',\''+item.estatus+'\')" class="waves-effect waves-light btn boton_general" ><i class="material-icons">mode_edit</i></a></td>'
                        html += '</tr>';                                
                    });                    
                }else{
                        
                }
                if(html == '') html = '<tr><td colspan="6" align="center">No se encontraron registros..</td></tr>'
                $("#"+tabla+" tbody").html(html);
                               
            }
      });
}
</script>
@endsection