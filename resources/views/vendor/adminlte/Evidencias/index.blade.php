@extends('adminlte::layouts.app')
@section('main-content')
    <div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-12 col-md-offset-0">
				<!--Default box -->
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title">
						Evidencias</h3>
						{{--<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
								<i class="fa fa-minus"></i></button>
							<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
								<i class="fa fa-times"></i></button>
						</div>--}}
					</div>
					<div class="box-body">
						<input value="0" id="persona_id" name="persona_id" class="form-control" type="hidden">
						<div class="col-md-12"><b>Seleccione su evidencia a subir, recuerde que solo podra subir documentos de tipo png y jpg.</b></div>
                        <div class="col-md-12">
                            <center><img src="{{ Gravatar::get($user->email) }}" class="img" alt="User Image" height="360" width="520"/></center>
                        </div>
                        <div class="col-md-12"  style=" height: 20px;overflow: hidden;"></div>
                        <div class="col-md-6">
							<div class="form-group with-icon">
								<label>Nombre de la evidencia:</label>
								<input  id="nombre" name="nombre" class="form-control" type="text">
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group with-icon">
								<label>Descripción</label>
								<input  id="nombre" name="nombre" class="form-control" type="text">
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group with-icon">
								<label>Fecha</label>
								<input  id="fecha" name="fecha" class="form-control" type="date">
							</div>
						</div>
                        <div class="col-md-6">
							<div class="form-group with-icon">
								<label>Categoria</label>
								<select class="form-control" id="estatus" name="estatus">
									<option value="0" selected disabled>Seleccione una opción</option>
                                    @for($i=0;$i< sizeof($categoria); $i++)
										<option value='{{$categoria[$i]['id']}}'   >{{$categoria[$i]['id']}} | {{$categoria[$i]['nombre']}}</option>
									@endfor
								</select>
								
							</div>
						</div>
                        <div class="col-md-6">
                            <label for="">Seleccionar Archivo:</label>
                            <a class="btn btn-primary btn-block" data-tooltip="Subir archivo">
                                <input type="file" id="archivo_id" onchange="SubirArchivo();" class="btn btn-primary btn-block">
                            </a>
                        </div>
                        <div class="col-md-12"  style=" height: 20px;overflow: hidden;"></div>
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->

			</div>
		</div>
	</div>
@endsection
@section('javascripts')
@parent
<script>
//SUBIR ARCHIVOS
/*function SubirArchivo(){
        var id = $('#id').val();
        var persona_id = $('#persona_id').val();//document.getElementById("archivo1");
        var categoria_id = $('#categoria_id').val();
        var fecha=$('#fecha').val();
        var descripcion=$('#descripcion').val();
        var nombre=$('#nombre').val();
        var files = $('#archivo_'+id)[0].files[0];
        Materialize.toast(files, 3000);
        var data = new FormData();
        
            data.append('id',id);  /// UN DATO ADICIONAL
            data.append('persona_id',persona_id);  /// UN DATO ADICIONAL
            data.append('categoria_id',categoria_id);
            data.append('nombre',nombre);
            data.append('descripcion',descripcion);
            data.append('archivo',files);
            data.append('fecha',fecha);
            data.append('_token','{{ csrf_token() }}');
            var url = '{{URL::to('/')}}/evidencia/guardar';
            Materialize.toast('Subiendo archivo', 3000);
        
            $.ajax({url:url, type:'POST', contentType:false, data:data,
                processData:false,cache:false,success: function(data, textStatus, jqXHR) {  
                    if(data!='PNG'||data!='JPG'){
                        Materialize.toast('El documento de ser formato de imagen', 3000);
                    }else{
                        Materialize.toast('Información actualizada correctamente', 3000);
                        $('#mostrara_'+data).show();
                    }
                }
         });
    }*/
    
function SubirArchivo() {
	var fecha = new Date();
	var persona_id=$('#persona_id').val();
	var nombre=$('#nombre').val();
	var descripcion=$('#descipcion').val();
    var categoria=$('#categoria').val();
    var doc_id=$('#doc_id').val();
	var form_data = new FormData(document.getElementById("doc_id"));
		form_data.append('persona_id',persona_id);
        form_data.append('fecha',fecha);
		form_data.append('nombre',nombre);
		form_data.append('descipcion',descipcion);
		form_data.append('categoria',categoria);
		form_data.append('doc_id',doc_id);
		form_data.append('_token','{{csrf_token()}}');
		    
			$.ajax({
				type: "POST",
				url: "{{URL::to('/')}}/evidencia/guardar",
				data: form_data,
				contentType: false,
				processData: false
			}).success(function(data){
				Materialize.toast('Fallo al subir el erchivo', 3000);
			}).fail(function(){
				Materialize.toast('Subido con exito', 3000);
			});
	}
}

</script>
@endsection