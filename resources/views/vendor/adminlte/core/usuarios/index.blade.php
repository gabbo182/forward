@extends('adminlte::layouts.app')
@section('main-content')
    <div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-12 col-md-offset-0">
				<!--Default box -->
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title">
						{{(isset($editar[0]['persona_id']) && $editar[0]['persona_id'] == 0)?'Nuevo usuario':'Editar usuario'}}</h3>
						{{--<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
								<i class="fa fa-minus"></i></button>
							<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
								<i class="fa fa-times"></i></button>
						</div>--}}
					</div>
					<div class="box-body">
					<form  action="{{URL::to('/usuario/guardar')}}" method="POST">
					{!! csrf_field() !!}
						<input value="{{isset($editar[0]['persona_id'])?$editar[0]['persona_id']:''}}" id="persona_id" name="persona_id" class="form-control" type="hidden">
						<input value="{{isset($editar[0]['persona_id'])?$editar[0]['persona_id']:''}}" id="usuario_id" name="cuenta_id" class="form-control" type="hidden">
                        <div class="col-md-4">
							<div class="form-group with-icon">
								<label>Nombre:</label>
								<input value="{{isset($editar[0]['nombre'])?$editar[0]['nombre']:''}}" id="nombre" name="nombre" class="form-control" type="text">
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group with-icon">
								<label>Primer apellido</label>
								<input value="{{isset($editar[0]['apaterno'])?$editar[0]['apaterno']:''}}" id="apaterno" name="apaterno" class="form-control" type="text">
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group with-icon">
								<label>Segundo apellido</label>
								<input value="{{isset($editar[0]['amaterno'])?$editar[0]['amaterno']:''}}" id="amaterno" name="amaterno" class="form-control" type="text">
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group with-icon">
								<label>Correo electrónico</label>
								<input value="{{isset($editar[0]['email'])?$editar[0]['email']:''}}" id="email" name="email" class="form-control" type="text">
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group with-icon">
								<label>Telefono</label>
								<input value="{{isset($editar[0]['telefono'])?$editar[0]['telefono']:''}}" id="tel" name="tel" class="form-control" type="text">
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group with-icon">
								<label>Telefono celular</label>
								<input value="{{isset($editar[0]['tel_celular'])?$editar[0]['tel_celular']:''}}" id="tel_cel" name="tel_cel" class="form-control" type="text">
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group with-icon">
								<label>Rol</label>
								<select class="form-control" id="rol_id">
									<option value="0" selected disabled>Seleccione una opción</option>
									@for($i=0;$i< sizeof($roles); $i++)
										<option value='{{$roles[$i]['rol_id']}}'   {{(isset($editar[0]['rol_id']) && $roles[$i]['rol_id']==$editar[0]['rol_id'])?"selected='selected'":''}}>{{$roles[$i]['rol_id']}} | {{$roles[$i]['nombre']}}</option>
									@endfor
								</select>
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group with-icon">
								<label>Usuario</label>
								<input value="{{isset($editar[0]['usuario'])?$editar[0]['usuario']:''}}" id="usuario" name="usuario" class="form-control" type="text">
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group with-icon">
								<label>Contraseña</label>
								<input id="pws" name="pws" class="form-control" type="password">
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group with-icon">
								<label>Estatus</label>
								<select class="form-control" id="estatus" name="estatus">
									<option value="0" selected disabled>Seleccione una opción</option>
									<option value="1">Activo</option>
									<option value="2">Inactivo</option>
								</select>
								
							</div>
						</div>
						<div class="col-md-12">
							<a class="btn btn-primary btn-block" type="submit" >Guardar</a>
						</div>
					</form>
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->

			</div>
		</div>
	</div>
@endsection
@section('javascripts')
@parent
<script>

</script>
@endsection