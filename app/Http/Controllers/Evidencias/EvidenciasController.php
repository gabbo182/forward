<?php
namespace App\Http\Controllers\Evidencias;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Password;
use App\Models\Core\Rol as rol;
use App\Models\Core\Cuenta as Cuentas;
use App\Models\Core\Persona as Persona;
use App\Models\Evidencia as Documento;
use App\Models\Categoria as Categoria;


class EvidenciasController extends Controller{
    public function index(Request $request,$id){
        $categoria=Categoria::Listado();
        
        return view('vendor.adminlte.Evidencias.index',['categoria'=>$categoria]);
    }
    
    public function listado(Request $request){
        
        return view('vendor.adminlte.Evidencias.listado');
    }
    
    public function guardar(Request $request){
		$fecha=$request->input('fecha');
		$persona_id=$request->input('persona_id');
        $doc_id=$request->input('doc_id');
		$file = Input::file('filestyle'.$doc_id);
		$categoria=$request->input('categoria');
		$descipcion=$request->input('descripcion');
		$nombre=$request->input('nombre');
		$ext=($file->clientExtension());

		$ruta=$persona_id.'/';
		$nombre = $file->getClientOriginalName();
		if( $ext != 'jpg' || $ext != 'png'){
			return "Formato no válido";
		}
		else{
			$name = $doc_id.".".$request->file('filestyle'.$doc_id)->clientExtension();
			$tipo_doc=$ext;
			Documento::Guardar($fecha,$persona_id,$doc_id,$categoria,$descipcion,$nombre,$ext,$ruta);
			$request->file('filestyle'.$doc_id)->move(storage_path($ruta), $name);

			return response('Archivo guardado correctamente');//(Storage::put($ruta,$name));
		}
	
    }
    
    public function listadojson(Request $request){   
        
        return response($datos);
    }
}