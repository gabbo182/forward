<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Models\Core\Cuenta as Cuentas;
use App\Models\Core\Rol as Rol;
//use App\Models\Core\Idiomas as Idiomas;
use App;

class InicioController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        //$this->middleware('guest'); 
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
            $user=Auth::user()->cuenta_id;
            //if(!$request->session()->has('usuario')){                                    
                        $cuenta = Cuentas::GetDatos($user);
                        //$modulos = Cuentas::permisos($user, $cuenta[0]['rol_id']);
                        $roles=Rol::ListadooJSON();
                        //$idiomas=Idiomas::ListadoActivos();
                        //$idiomas=(is_object($idiomas)?array():$idiomas);
                        $request->session()->put('usuario',$cuenta[0]);
                        //$request->session()->put('modulos',$modulos);
                        $request->session()->put('roles',$roles);
                        //$request->session()->put('idiomas',$idiomas);
                        //$request->session()->put('idioma_act','mx.svg');
                        //if(strlen($cuenta[0]['idioma'])==0){$cuenta[0]['idioma']='es'; }
                        //$request->session()->put('idioma_clave',$cuenta[0]['idioma']);
                        
        //return redirect($cuenta[0]['rol_rutainicial']); 
        return redirect('/inicio'); 
        return view('inicio',['datos'=>$modulos]);
    }
    public function inicio(Request $request)
    {       
        return view('vendor.adminlte.home',[]);
    }
}