<?php
namespace App\Http\Controllers\Core;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Password;
use App\Models\Core\Rol as rol;
use App\Models\Core\Cuenta as Cuentas;
use App\Models\Core\Persona as Persona;


class UsuariosController extends Controller
{
    public function index(Request $request,$id){
        $editar=Cuentas::GetDatos($id);
        $rol=rol::ListadooJSON();
        return view('vendor.adminlte.core.usuarios.index',['editar'=>$editar,'roles'=>$rol]);
    }
    
    public function listado(Request $request){
        return view('vendor.adminlte.core.usuarios.listado');
    }
    
    public function guardar(Request $request){
        
        $usuario_id=$request->input('cuenta_id'); 
        $persona_id=$request->input('persona_id');
        $nombre=$request->input('nombre');
        $apaterno=$request->input('apaterno');
        $amaterno=$request->input('amaterno'); 
        $telefono=$request->input('telefono');
        $usuario=$request->input('usuario');
        $password=$request->input('pws');
        $rol_id=$request->input('rol_id');
        $email=$request->input('email');
        $estatus=$request->input('estatus');
        
        
        if($usuario_id==0){
                $usuario_id=  Cuentas::max('cuenta_id');      
                $usuario_id=$usuario_id+1;
                $Usuario = new Cuentas;
                $persona_id=  Persona::max('persona_id');
                $persona_id=$persona_id+1;
                $Persona = new Persona;
                $Usuario->cuenta_id=$usuario_id;
                $Usuario->persona_id=$persona_id;
                $Persona->persona_id=$persona_id;
                
        }else{
             $Usuario = Cuentas::where('cuenta_id' ,'=', $usuario_id)->first();
             $Usuario->cuenta_id=$usuario_id;
             $Usuario->persona_id=$persona_id;
             
             $Persona = Persona::where('persona_id' ,'=', $persona_id)->first();
             $Persona->persona_id=$persona_id;             
        }
        
        $Usuario->usuario=$usuario;
        $Usuario->estatus=$estatus;
        $Usuario->rol_id=$rol_id;
        $Usuario->cuenta_id=$usuario_id;
        $Usuario->email=$email;
        $Usuario->remember_token = str_random(60);
        
         if(strlen($password)){
            $Usuario->password = bcrypt($password);            
        }
 
        $Persona->nombre=$request->input('nombre');
        $Persona->apaterno=$request->input('apaterno');
        $Persona->amaterno=$request->input('amaterno');
        $Persona->telefono=$request->input('telefono');
        $Persona->tel_celular=$request->input('tel_celular');
        
        $Persona->save();        
        $Usuario->save();
        $data['id'] =$usuario_id;
        return redirect("{{URL('to')}}/usuarios/editar/".$usuario_id);
//        return ($data);       
    }
    
    public function listadojson(Request $request){   
        $resultados=Cuentas::listadojson();
        $datos['datos']=(sizeof($resultados) > 0)?$resultados:array();
        return response($datos);
    }
   
}