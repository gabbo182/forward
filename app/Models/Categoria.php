<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Categoria extends Model
{
    protected $table = 'evidencias.categoria';
    protected $primaryKey = 'id';
    
    public function scopeListado($query){
        $datos=$query->select('id','nombre')
            ->get()->toArray();
        return $datos;
    }
}