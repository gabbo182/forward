<?php

namespace App\Models\Core;

use Illuminate\Database\Eloquent\Model;

class Rol extends Model
{
     protected $table = 'core.trol';
     protected $primaryKey = 'rol_id';
     
     public function scopeListadooJSON($query){          
         $datos = $query
         ->select('rol_id','nombre')
         ->orderBy('rol_id','desc')->get()->toArray(); 
         $datos=(sizeof($datos) > 0)?$datos:array();
         return $datos; 
     }
     public function scopeListadoJSON($query, $page, $rows, $search, $draw,$id){          
         $resultados = $query
         //->join('core.trol_tipo','')
         ->select('core.trol.rol_id','core.trol.tipo_id','core.trol.rol_nombre'
                  ,'core.trol.perfil'
                  ,'core.trol.rol_descripcion','core.trol.rol_siglas','core.trol.rol_rutainicial'
                  ,'core.trol.rol_estatus',
			   \DB::raw('(select core.trol_tipo.tipo_nombre from core.trol_tipo where
					  core.trol_tipo.tipo_id=core.trol.tipo_id
					   ) as tipo_nombre')
			   )
         ->where('core.trol.tipo_id', $id)
         ->orderBy('core.trol.rol_id','desc')
         ->limit($rows)->offset($page)->get()->toArray();
         
         $totales=Rol::select(\DB::raw('count(rol_id) as total'))->where('core.trol.tipo_id', $id)->get()->toArray();		  		
         $datos['data']=(sizeof($resultados) > 0)?$resultados:array();
         $datos['recordsTotal']=(isset($totales[0]['total']))?$totales[0]['total']:0;
	    $datos['search'] = $search['value'];
	    $datos['draw'] = (int)$draw;
	    $datos['recordsFiltered'] = (isset($totales[0]['total']))?$totales[0]['total']:0;
        return $datos;
     }
    public function scopeListadoJSON2($query){          
         $resultados = $query
         //->join('core.trol_tipo','')
         ->select('core.trol.rol_id','core.trol.tipo_id','core.trol.rol_nombre'
                  ,'core.trol.perfil'
                  ,'core.trol.rol_descripcion','core.trol.rol_siglas','core.trol.rol_rutainicial'
                  ,'core.trol.rol_estatus',
			   \DB::raw('(select core.trol_tipo.tipo_nombre from core.trol_tipo where
					  core.trol_tipo.tipo_id=core.trol.tipo_id
					   ) as tipo_nombre')
			   ) 
         ->orderBy('core.trol.rol_id','desc')->get()->toArray(); 
        return $resultados;
     }	
     public function scopeListado($query,$id){          
         $resultados = $query
         ->join('core.trol_tipo','core.trol_tipo.tipo_id','=','core.trol.tipo_id')
         ->select('core.trol.rol_id','core.trol.tipo_id','core.trol.rol_nombre'
                  ,'core.trol_tipo.tipo_nombre','core.trol.perfil'
                  ,'core.trol.rol_descripcion','core.trol.rol_siglas','core.trol.rol_rutainicial'
                  ,'core.trol.rol_estatus')
         ->where('core.trol.tipo_id', $id)
         ->orderBy('core.trol.rol_id','desc')->get()->toArray();         
        return $resultados;
     }     
     public function scopeGetDatos($query,$id){          
         $datos = $query
         ->join('core.trol_tipo','core.trol_tipo.tipo_id','=','core.trol.tipo_id')
         ->select('core.trol.rol_id','core.trol.tipo_id','core.trol.rol_nombre'
                  ,'core.trol.rol_descripcion','core.trol.rol_siglas','core.trol.rol_rutainicial'
                  ,'core.trol.rol_estatus')
         ->where('core.trol.rol_id','=',$id)
         ->get()->toArray(); 
         $datos=(sizeof($datos) > 0)?$datos:array(); 
         return $datos; 
     }
     
     public function scopeConsultaJSON($query,$id,$nopagina, $paginaactual){
         $inicio = ($nopagina * $paginaactual) - $paginaactual;
         $resultados = $query
         ->join('core.trol_tipo','core.trol_tipo.tipo_id','=','core.trol.tipo_id')
         ->select('core.trol.rol_id','core.trol.tipo_id','core.trol.rol_nombre'
                  ,'core.trol.rol_descripcion','core.trol.rol_siglas','core.trol.rol_rutainicial'
                  ,'core.trol.rol_estatus')
         ->where('core.trol.rol_id','=',$id)
         ->orderBy('core.trol.rol_id','desc')
         ->limit($paginaactual)->offset($inicio)->get()->toArray();
         
         $totales=trol::select(\DB::raw('count(rol_id) as total'))->get()->toArray();
           
         $datos['datos']=(sizeof($resultados) > 0)?$resultados:array();
         $datos['total']=(isset($totales[0]['total']))?$totales[0]['total']:0; 
         return $datos;
     }

     public function scopeGuardar($query,$rol_id,$tipo_id,$rol_nombre,$rol_descripcion,$rol_siglas,$rol_rutainicial,$rol_estatus){
          if($rol_id==0){
               $documento = New trol(); 
          }else{
               $documento = trol::find($rol_id);
          }
          
          $documento->tipo_id=$tipo_id;
          $documento->rol_nombre=$rol_nombre;
          $documento->rol_descripcion=$rol_descripcion;
          $documento->rol_siglas=$rol_siglas;
          $documento->rol_rutainicial=$rol_rutainicial;
          $documento->rol_estatus=$rol_estatus;
          $documento->save();
     }
     
     public function scopeEliminar($query,$rol_id){          
          $documento = trol::findOrFail($rol_id);
          $documento->delete();
     }   
     public function scopeBuscarol($query,$id){          
          $datos = $query->select('core.trol.rol_id','core.trol.tipo_id',
                                  'core.trol.rol_nombre',
                                  'core.trol.rol_estatus',
						    'core.trol.perfil',
						    'core.trol.perfil',
						    'core.trol.rol_cursobase', 
                                  'core.trol.rol_rutainicial')
          ->orderBy('core.trol.rol_nombre', 'desc')
          ->whereIn('core.trol.rol_estatus',array(1,2,0))->where('rol_id',$id)->get()->toArray(); 
          $datos=(sizeof($datos) > 0)?$datos:array();
		for($i=0; $i< sizeof($datos); $i++){
			$datos[$i]['perfil']=json_decode($datos[$i]['perfil']); 
		}
          return $datos; 
     }
     public function scopeListadodetalle($query,$id, $tipo_id){          
               $datosr = \DB::select(\DB::raw('select a.modulo_id, a.nombre, a.icon, a.categoria_nombre,									
               b.rold_acceso,b.rold_lectura,b.rold_escritura,b.rold_eliminar                          
               from 
               (select core.tmodulos.modulo_id,core.tmodulos_categoria.categoria_nombre as categoria_nombre,              
               core.tmodulos.modulo_nombre as nombre,core.tmodulos.modulo_icono as icon
               from core.tmodulos_categoria, core.tmodulos
               where core.tmodulos_categoria.categoria_id=core.tmodulos.categoria_id) as a
			left join core.trol_detalle as b 
               on a.modulo_id=b.modulo_id
               and b.rol_id='.$id));
			$datos['datos']=(sizeof($datosr) > 0)?$datosr:array();
			$datos['total']=(isset($datosr))?sizeof($datosr):0;  
               return $datos;
     }
     public function scopeCambiapermiso($query,$modulo,$opc,$rol,$valor){
           $permiso = RolDetalle::select('roldetalle_id')->where('modulo_id',$modulo)->where('rol_id',$rol)->get()->toArray();
		 //
           $opcion='';
           switch($opc){
               case '1': $opcion='acceso'; break;
               case '2': $opcion='escritura'; break;
               case '3': $opcion='eliminar'; break;
           }
		 if($valor=='true'){$valor=1; }else{ $valor=0; }
		 
           if(isset($permiso[0]['roldetalle_id'])){
               RolDetalle::where('roldetalle_id',$permiso[0]['roldetalle_id'])->
			  update(array("rold_".$opcion =>$valor,
						'permisos'=>\DB::raw("jsonb_set(permisos,'{rold_".$opcion."}','".$valor."')")));
           }else{
		     RolDetalle::insert(array('rol_id' => $rol, 'modulo_id' =>
								$modulo,"rold_".$opcion =>$valor,'permisos'=>
								\DB::raw("'{\"rold_".$opcion."\":".$valor."}'")));
           }
     }
	function scopeAjustaPerfil($query,$opc,$valor,$rol){
		 Rol::where('rol_id',$rol)->update(array('perfil'=>\DB::raw("jsonb_set(perfil,'{".$opc."}','".$valor."')")));
	}
	
	public function scopeRolesPerfil($query, $page, $rows, $search, $draw)
	{
		$listado = $query
         ->join('core.trol_tipo','core.trol_tipo.tipo_id','=','core.trol.tipo_id')
         ->select('core.trol.rol_id','core.trol.tipo_id','core.trol.rol_nombre'
                  ,'core.trol_tipo.tipo_nombre','core.trol.perfil'
                  ,'core.trol.rol_descripcion','core.trol.rol_siglas','core.trol.rol_rutainicial'
                  ,'core.trol.rol_estatus')         
         ->orderBy('core.trol.rol_id','desc')
         ->limit($rows)->offset($page)->get()->toArray();
		$total = Rol::join('core.trol_tipo','core.trol_tipo.tipo_id','=','core.trol.tipo_id')->count();
		if(is_array($listado)){
			for($i=0; $i< sizeof($listado); $i++){				
				$listado[$i]['perfil'] = json_decode($listado[$i]['perfil']); 
			}
		}
		$datos['data']=(sizeof($listado) > 0)?$listado:array();
		$datos['recordsTotal'] = $total;
		$datos['search'] = $search['value'];
		$datos['draw'] = (int)$draw;
		$datos['recordsFiltered'] = $total;		
		return $datos;
	}
	
	public function scopeRolporcurso($query,$curso_id){
		$rol=$query->select('rol_id')->where('rol_cursobase',$curso_id)->get()->toArray();
		return $rol;
	}
}