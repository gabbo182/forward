<?php

namespace App\Models\Core;

use Illuminate\Database\Eloquent\Model;

class Cuenta extends Model
{
     protected $table = 'core.tcuenta';
     protected $primaryKey = 'cuenta_id';
     
     public function scopeListadoJSON($query){
         $datos = $query
         ->join('core.tpersona','core.tpersona.persona_id','=','core.tcuenta.persona_id')
         ->join('core.trol','core.trol.rol_id','=','core.tcuenta.rol_id')
         ->select('core.tcuenta.cuenta_id','core.tcuenta.persona_id', 'core.tcuenta.plantel_id'
                  ,'core.tcuenta.rol_id','core.tcuenta.email','core.tcuenta.estatus'
                  ,'core.trol.rol_nombre','core.tpersona.per_nombre as nombre'
                  ,'core.tpersona.per_primer_apellido as apellido1','core.tpersona.per_segundo_apellido as apellido2')
         ->where('core.tcuenta.rol_id','=',1)
         ->orderBy('core.tcuenta.cuenta_id','desc')->get()->toArray();
         $datos=(sizeof($datos) > 0)?$datos:array(); 
         return $datos; 
     }
     
     public function scopeDatosInscrito($query, $id){
          $datos=$query->select('*')->where('persona_id',$id)->get()->toArray();
          return $datos;
     }
     public function listadojson($query){
         $datos = $query
         ->join('core.tpersona','core.tpersona.persona_id','=','core.tcuenta.persona_id')
         ->join('core.trol','core.trol.rol_id','=','core.tcuenta.rol_id')
         ->select('core.tpersona.persona_id as id',
                  'core.tpersona.nombre',
                  'core.tpersona.apaterno',
                  'core.tpersona.amaterno',
                  'core.tpersona.telefono',
                  'core.tpersona.tel_celular',
                  'core.tpersona.email',
                  'core.tcuenta.rol_id',
                  'core.tcuenta.cuenta_id',
                  'core.tcuenta.usuario',
                  'core.tcuenta.estatus')
         ->get()->toArray();
         $datos=(sizeof($datos) > 0)?$datos:array(); 
         return $datos; 
     }
     
     
     public function scopeGetDatos($query,$id){          
         $datos = $query
         ->join('core.tpersona','core.tpersona.persona_id','=','core.tcuenta.persona_id')
         ->join('core.trol','core.trol.rol_id','=','core.tcuenta.rol_id')
         ->select('core.tpersona.persona_id as id',
                  'core.tpersona.nombre',
                  'core.tpersona.apaterno',
                  'core.tpersona.amaterno',
                  'core.tpersona.telefono',
                  'core.tpersona.tel_celular',
                  'core.tpersona.email',
                  'core.tcuenta.rol_id',
                  'core.tcuenta.cuenta_id',
                  'core.tcuenta.usuario',
                  'core.tcuenta.estatus')
         ->where('core.tcuenta.cuenta_id','=',$id)
         ->get()->toArray();
         $datos=(is_array($datos))?$datos:array(0=>array('cuenta_id'=>0)); 
         return $datos; 
     }

     public function scopeGuardar($query,$cuenta_id,$persona_id,$rol_id,$cta_usuario,$cta_clave,$cta_email,$cta_estatus){
          if($cuenta_id==0){
                $documento = New tcuenta(); 
           }else{
                $documento = tcuenta::find($cuenta_id);
           }
         $documento->persona_id=$persona_id;
         $documento->rol_id=$rol_id;
         $documento->cta_usuario=$cta_usuario;
         $documento->cta_clave=$cta_clave;
         $documento->cta_email=$cta_email;
         $documento->cta_estatus=$cta_estatus;
         $documento->save();
     }

     public function scopeEliminar($query,$id){
          $documento = Cuenta::where('cuenta_id', $id)->delete();
     }
     /*
     
      public function scopePermisos($query, $id){ 
           $queryTemp = $query;
           //\DB::enableQueryLog();
           $menu = $query
           ->join('core.trol', 'core.trol.rol_id', '=', 'core.tcuenta.rol_id')
           ->join('core.trol_detalle', 'core.trol_detalle.rol_id', '=', 'core.trol.rol_id')
           ->join('core.tmodulos', 'core.tmodulos.modulo_id', '=', 'core.trol_detalle.modulo_id')           
           ->join('core.tmodulos_categoria', 'core.tmodulos_categoria.categoria_id', '=', 'core.tmodulos.categoria_id')           
           ->join('core.taplicacion', 'core.taplicacion.aplicacion_id', '=', 'core.tmodulos_categoria.aplicacion_id')           
           ->select('core.tcuenta.cuenta_id', 'core.tcuenta.plantel_id',
                    'core.taplicacion.aplicacion_id',
                    'core.taplicacion.aplicacion_nombre',
                    'core.taplicacion.aplicacion_icono',   
                    'core.tmodulos_categoria.categoria_nombre as categoria',
                    'core.tmodulos_categoria.categoria_id',
                    'core.tmodulos.modulo_nombre as modulo',
                    'core.tmodulos.modulo_descripcion as descripcion',
                    'core.tmodulos.modulo_estatus', 'core.trol.rol_nombre as rol', 'core.tmodulos.modulo_ruta',
                    'core.tmodulos.modulo_id',
                    'core.tmodulos.modulo_icono',
                    'core.tmodulos.modulo_variable',
                    'core.taplicacion.aplicacion_variable',
                    'core.tmodulos_categoria.categoria_variable',
                    'core.tmodulos_categoria.categoria_icono',
                    'core.tmodulos_categoria.categoria_orden as corden',                    
                    'core.trol_detalle.rold_acceso', 'core.trol_detalle.rold_lectura',
                    'core.trol_detalle.rold_escritura', 'core.trol_detalle.rold_eliminar',                     
                    'core.tmodulos.modulo_orden')
           ->orderBy('core.taplicacion.aplicacion_id')
           ->orderBy('core.tmodulos_categoria.categoria_id')
           ->orderBy('core.tmodulos_categoria.categoria_orden')
           ->orderBy('core.tmodulos.modulo_orden', 'desc')
           ->orderBy('core.tmodulos.modulo_id', 'desc')
           ->where('core.tcuenta.cuenta_id','=',$id)
               ->where('core.trol_detalle.rold_acceso','=',1)

               //->where('core.trol_detalle.rold_acceso',$rol)
           ->where('core.tmodulos.modulo_estatus','=','1')
           ->where('core.tmodulos_categoria.categoria_id','>','0')
           ->where('core.tmodulos.modulo_menu','=','1')->get()->toArray();

/*
          //$query = \DB::getQueryLog();           $lastQuery = end($query);           dd($lastQuery);

           $menu2 = Cuenta::join('core.trol', 'core.trol.rol_id', '=', 'core.tcuenta.rol_id')
           ->join('core.trol_detalle', 'core.trol_detalle.rol_id', '=', 'core.trol.rol_id')
           ->join('core.tmodulos', 'core.tmodulos.modulo_id', '=', 'core.trol_detalle.modulo_id')
           ->join('core.tmodulos_categoria', 'core.tmodulos_categoria.categoria_id', '=', 'core.tmodulos.categoria_id')
           ->join('core.taplicacion', 'core.taplicacion.aplicacion_id', '=', 'core.tmodulos_categoria.aplicacion_id')           
           ->select('core.tcuenta.cuenta_id',
                    'core.taplicacion.aplicacion_id',
                    'core.taplicacion.aplicacion_nombre',    
                    'core.tmodulos_categoria.categoria_nombre as categoria','core.tmodulos_categoria.categoria_id',
                    'core.tmodulos.modulo_nombre as modulo',
                    'core.tmodulos.modulo_descripcion as descripcion',
                    'core.tmodulos.modulo_estatus', 'core.trol.rol_nombre as rol', 'core.tmodulos.modulo_ruta',
                    'core.tmodulos.modulo_icono', 'core.tmodulos_categoria.categoria_icono as icon_categoria','core.tmodulos.modulo_orden as corden','core.tmodulos.modulo_orden',
                    'core.trol_detalle.rold_acceso', 'core.trol_detalle.rold_lectura', 'core.trol_detalle.rold_escritura', 'core.trol_detalle.rold_eliminar'
                    )
           ->orderBy('core.taplicacion.aplicacion_id')
           ->orderBy('core.tmodulos_categoria.categoria_id')           
           ->orderBy('core.tmodulos_categoria.categoria_orden')
           ->orderBy('core.tmodulos.modulo_orden', 'desc')
           ->where('core.tcuenta.cuenta_id','=',$id)
           ->where('core.trol_detalle.rold_acceso',1)
           ->where('core.tmodulos.modulo_estatus','=','1')
           ->where('core.tmodulos_categoria.categoria_id','=','0')
           ->where('core.tmodulos.modulo_menu','=','1')->get()->toArray();

           $datos=$menu; //array_merge($menu2,$menu);
        
           $n=sizeof($datos);
           for ($i = 0; $i < $n; $i++) {
                if(round($datos[$i]['corden'],0)==0){ $datos[$i]['corden']=1000; $datos[$i]['orden']=1000; }
           }
           for ($i = 1; $i < $n; $i++) {
                if(round($datos[$i]['corden'],0)==0){$datos[$i]['corden']='1000'; }
                for ($j = 0; $j < $n - $i; $j++) {
                 if (round($datos[$j]['corden'],0) > round($datos[$j + 1]['corden'],0)) {
                   $k = $datos[$j + 1];
                   $datos[$j + 1] = $datos[$j];
                   $datos[$j] = $k;
                 }
               }
             }
           $datos=(sizeof($datos)>0)?$datos:array(0=>array('categoria'=>''));
           return $datos;
      }*/

}
