<?php

namespace App\Models\Core;
use Laravel\Scout\Searchable;
use Illuminate\Database\Eloquent\Model;

class Persona extends Model
{

     use Searchable;
     protected $table = 'core.tpersona';
     protected $primaryKey = 'persona_id';
     public $asYouType = true;
        /**
     * Get the indexable data array for the model.
     *
     * @return array
     */

     public function toSearchableArray()
     {
        $array = $this->toArray();
        // Customize array...
        return $array;
     }
  /*   public function searchableAs()
     {
         return 'core.tpersona';
     }*/
     public function scopeDatosInscripcion($query, $id){
          $datos=$query->join('prospectos.datos','prospectos.datos.persona_id','=','core.tpersona.persona_id')
          ->join('prospectos.hijos','prospectos.hijos.id','=','prospectos.datos.hijos_id')
          ->select('core.tpersona.persona_id','per_nombre as nombre','per_primer_apellido as apaterno'
                    ,'per_segundo_apellido as amaterno','prospectos.hijos.nivel_escolar as nivel',
                    'prospectos.hijos.grado_academico as grado')
          ->where('core.tpersona.persona_id',$id)->get()->toArray();
          return $datos;
     }

     public function scopeDatos($query,$id){
          
          $datos=$query
          ->leftjoin('core.tcuenta','core.tcuenta.persona_id','=','core.tpersona.persona_id')
          ->leftjoin('core.tpersonal','core.tpersonal.persona_id','=','core.tpersona.persona_id')
          ->leftjoin('catalogos.testado','catalogos.testado.estado_id','=','core.tpersona.estado_id')
          ->leftjoin('catalogos.tmunicipio','catalogos.tmunicipio.municipio_id','=','core.tpersona.municipio_id')
          ->leftjoin('catalogos.tlocalidad','catalogos.tlocalidad.localidad_id','=','core.tpersona.localidad_id')
          ->leftjoin('catalogos.testado_civil','catalogos.testado_civil.edocivil_id','=','core.tpersona.edocivil_id')
          ->leftjoin('core.trol','core.trol.rol_id','=','core.tcuenta.rol_id')
          ->select('core.tpersona.persona_id','per_nombre','per_primer_apellido','per_segundo_apellido',
                    'per_rfc','core.tpersona.edocivil_id','per_curp','per_telefono','per_celular','core.tcuenta.tutor_id as tutor',
                    'per_email','per_calle','per_colonia','per_domicilio_cp','per_sexo','per_foto',
                    'core.tpersona.estado_id','per_alias','core.tpersona.municipio_id',
                    'core.tpersona.localidad_id','per_lugar_nacimiento',//'core.tpersonal.per_noempleado',
                    'per_num','core.tcuenta.usuario','catalogos.testado_civil.edocivil_nombre',
                    'catalogos.testado.nom_ent as estado','catalogos.tmunicipio.nom_ent as mun',
                    'catalogos.tlocalidad.nom_ent as loc','core.trol.rol_nombre'
                    ,\DB::raw("to_char(per_fecha_nacimiento, 'DD/MM/YYYY') as per_fecha_nacimiento")
                  , \DB::raw('concat(per_nombre,\' \',per_primer_apellido,\' \',per_segundo_apellido) as nom_alumno'))
          ->where('core.tpersona.persona_id',$id)->get()->toArray();
          
          return $datos;
     }
     
     public function scopeDatos2($query,$id){
          $datos=$query
          ->leftjoin('core.tcuenta','core.tcuenta.persona_id','=','core.tpersona.persona_id')
          ->leftjoin('core.tpersonal','core.tpersonal.persona_id','=','core.tpersona.persona_id')
          
          ->select('core.tpersona.persona_id','per_nombre','per_primer_apellido','per_segundo_apellido',
                    'per_rfc','edocivil_id','per_curp','per_telefono','per_celular',
                    'core.tcuenta.tutor_id as tutor','per_email','per_calle','per_colonia',
                    'per_domicilio_cp','per_sexo','per_foto','estado_id','per_alias','municipio_id',
                    'localidad_id','per_lugar_nacimiento','core.tpersonal.per_noempleado','per_num'
                    ,\DB::raw("to_char(per_fecha_nacimiento, 'DD/MM/YYYY') as per_fecha_nacimiento"))
          ->where('core.tpersona.persona_id',$id)->get()->toArray();
          
          return $datos;
     }
     
     public function scopeListadoJSON($query){
         $datos = $query
         ->select('persona_id','edocivil_id','per_nombre','per_primer_apellido','per_segundo_apellido'
                  ,'per_rfc','per_curp','per_telefono','per_celular','per_email','per_calle','per_colonia'
                  ,'per_domicilio_cp','per_sexo','per_foto','per_fecha_nacimiento','estado_id','municipio_id'
                  ,'localidad_id','per_lugar_nacimiento')
         ->orderBy('persona_id','desc')->get()->toArray();
         $datos=(sizeof($datos) > 0)?$datos:array();
         return $datos;
     }

     public function scopeGetDatos($query,$id){
         $datos = $query
         ->select('persona_id','edocivil_id','per_nombre','per_primer_apellido','per_segundo_apellido'
                  ,'per_rfc','per_curp','per_telefono','per_celular','per_email','per_calle','per_colonia'
                  ,'per_domicilio_cp','per_sexo','per_foto','per_fecha_nacimiento','estado_id','municipio_id'
                  ,'localidad_id','per_lugar_nacimiento')
         ->where('persona_id','=',$id)
         ->get()->toArray();
         $datos=(sizeof($datos) > 0)?$datos:array();
         return $datos;
     }

     public function scopeGetDatosDocente($query,$id){
         $datos = $query
         ->select('persona_id','edocivil_id',\DB::raw('concat(per_nombre,\' \',per_primer_apellido,\' \',per_segundo_apellido) as nombre')
                  ,'per_rfc','per_curp','per_telefono','per_celular','per_email','per_calle','per_colonia'
                  ,'per_domicilio_cp','per_sexo','per_foto','per_fecha_nacimiento','estado_id','municipio_id'
                  ,'localidad_id','per_lugar_nacimiento')
         ->where('persona_id','=',$id)
         ->get()->toArray();
         $datos=(sizeof($datos) > 0)?$datos:array();
         return $datos;
     }

     public function scopeConsultaJSON($query,$id,$nopagina, $paginaactual){
         $inicio = ($nopagina * $paginaactual) - $paginaactual;
         $resultados = $query
         ->select('persona_id','edocivil_id','per_nombre','per_primer_apellido','per_segundo_apellido'
                  ,'per_rfc','per_curp','per_telefono','per_celular','per_email','per_calle','per_colonia'
                  ,'per_domicilio_cp','per_sexo','per_foto','per_fecha_nacimiento','estado_id','municipio_id'
                  ,'localidad_id','per_lugar_nacimiento')
         ->where('persona_id','=',$id)
         ->orderBy('persona_id','desc')
         ->limit($paginaactual)->offset($inicio)->get()->toArray();

         $totales=Persona::select(\DB::raw('count(persona_id) as total'))->get()->toArray();

         $datos['datos']=(sizeof($resultados) > 0)?$resultados:array();
         $datos['total']=(isset($totales[0]['total']))?$totales[0]['total']:0;
         return $datos;
     }

     public function scopeGuardar($query,$persona_id,$edocivil_id,$per_nombre,$per_primer_apellido
                                  ,$per_segundo_apellido,$per_rfc,$per_curp,$per_telefono,$per_celular
                                  ,$per_email,$per_calle,$per_colonia,$per_domicilio_cp,$per_sexo
                                  ,$per_foto,$per_fecha_nacimiento,$estado_id,$municipio_id,$localidad_id
                                  ,$per_lugar_nacimiento,$per_alias,$reg_alimenticio){
          if($persona_id==0){
                $documento = New tpersona();
           }else{
                $documento = Persona::find($persona_id);
           }

          $documento->edocivil_id=$edocivil_id;
          $documento->per_nombre=$per_nombre;
          $documento->per_primer_apellido=$per_primer_apellido;
          $documento->per_segundo_apellido=$per_segundo_apellido;
          $documento->per_rfc=$per_rfc;
          $documento->per_curp=$per_curp;
          $documento->per_telefono=$per_telefono;
          $documento->per_celular=$per_celular;
          $documento->per_email=$per_email;
          $documento->per_calle=$per_calle;
          $documento->per_colonia=$per_colonia;
          $documento->per_domicilio_cp=$per_domicilio_cp;
          $documento->per_sexo=$per_sexo;
          if($per_foto!=null){
          $documento->per_foto=$per_foto;
          }
          $documento->per_fecha_nacimiento=$per_fecha_nacimiento;
          $documento->estado_id=$estado_id;
          $documento->municipio_id=$municipio_id;
          $documento->localidad_id=$localidad_id;
          $documento->per_alias=$per_alias;
          if($per_lugar_nacimiento!=null){
          $documento->per_lugar_nacimiento=$per_lugar_nacimiento;
          }
          $documento->reg_alimenticio=$reg_alimenticio;
          $documento->save();

          return $documento->persona_id;
     }

     public function scopeGuardarAlumno($query,$persona_id,$nombre,$apellido_1,$apellido_2,$fecha_nac,$estado_id,
								$municipio_id,$localidad_id,$vive,$genero_id,$calle,$cp){
          if($persona_id==0){
               $documento = New Persona();
          }else{
               $documento = Persona::find($persona_id);
          }
          $documento->per_nombre=$nombre;
          $documento->per_primer_apellido=$apellido_1;
          $documento->per_segundo_apellido=$apellido_2;
          $documento->per_calle=$calle;
          $documento->per_domicilio_cp=$cp;
          $documento->per_sexo=$genero_id;
          $documento->per_fecha_nacimiento=$fecha_nac;
          $documento->estado_id=$estado_id;
          $documento->municipio_id=$municipio_id;
          $documento->localidad_id=$localidad_id;
          $documento->per_lugar_nacimiento=$vive;
          $documento->save();
          return $documento->persona_id;
     }

     public function scopeAlumno($query,$ids){
          $datos=$query->select('persona_id','per_foto')->where('persona_id',$ids)->get()->toArray();
          $datos=is_array($datos)?$datos:array();
          return $datos;
     }

     public function scopeEliminar($query,$persona_id){

          $documento = Persona::findOrFail($persona_id);
          $documento->delete();
     }

      public function scopeListadoJSONcuenta($query,$rol,$id){
         $datos = $query
         ->join('core.tcuenta','core.tcuenta.persona_id','=','core.tpersona.persona_id')
         ->select('core.tpersona.persona_id','edocivil_id','per_nombre','per_primer_apellido','per_segundo_apellido'
                  ,'per_rfc','per_curp','per_telefono','per_celular','per_email','per_calle','per_colonia'
                  ,'per_domicilio_cp','per_sexo','per_foto','per_fecha_nacimiento','estado_id','municipio_id'
                  ,'localidad_id','per_lugar_nacimiento')

         ->orderBy('persona_id','asc');
     if($rol!=1){
         $datos =$datos->where('core.tpersona.persona_id','=',$id);
     }

         $datos =$datos->get()->toArray();
         $datos=(sizeof($datos) > 0)?$datos:array();
         return $datos;
     }

      public function scopebuscardocente($query,$buscar){
         $datos = $query

         ->join('core.tcuenta','core.tcuenta.persona_id','=','core.tpersona.persona_id')
         ->join('core.trol','core.trol.rol_id','=','core.tcuenta.rol_id')
         ->select('core.tpersona.persona_id as id',
                  \DB::raw('concat(per_nombre,\' \',per_primer_apellido,\' \',per_segundo_apellido) as nom')
                  )
         ->where(\DB::raw('concat(per_nombre,\' \',per_primer_apellido,\' \',per_segundo_apellido)'),'ILIKE',"%".$buscar."%")
         ->where('tipo_id','=',2)
         ->orderBy('core.tpersona.persona_id','asc');
         $datos =$datos->get()->toArray();
         $datos=(sizeof($datos) > 0)?$datos:array();
         return $datos;
     }
     //Funcion para complementar TNTSearch
     public function scopeListadoBuscador($query,$ids,$operador,$opcion){
          /*
          $personas=$query->leftJoin('core.tcuenta','core.tcuenta.persona_id','=','core.tpersona.persona_id')
		 ->leftJoin('core.trol','core.trol.rol_id','=','core.tcuenta.rol_id')
		 ->leftJoin('core.tpersonal','core.tpersonal.persona_id','=','core.tpersona.persona_id')
		 ->leftJoin('ce.talumno','ce.talumno.persona_id','=','core.tpersona.persona_id')
		 //->leftJoin('catalogos.tpuesto','catalogos.tpuesto.puesto_id','=','core.tpersonal.puesto_id')
		 ->select(\DB::raw('concat(core.tpersona.per_nombre,\' \',core.tpersona.per_primer_apellido,\' \',core.tpersona.per_segundo_apellido) as consulta')
				  ,'core.tpersona.persona_id as id','core.trol.rol_nombre as rol','core.trol.rol_id','core.tpersona.per_rfc as rfc','core.tpersona.per_foto as foto'
				 // ,'catalogos.tpuesto.puesto_nombre as puesto'
				  ,\DB::raw("(select catalogos.tpuesto.puesto_nombre from catalogos.tpuesto where catalogos.tpuesto.puesto_id = core.tpersonal.puesto_id) as puesto")
				  ,\DB::raw("(select catalogos.tcurso.cur_nombre from catalogos.tcurso where catalogos.tcurso.curso_id = ce.talumno.curso_id) as curso")
				  ,\DB::raw("(select ce.tgrupos.grupo_nombre from ce.tgrupos where ce.tgrupos.grupo_id = ce.talumno.grupo_id) as grupo")
				  ,\DB::raw("(select catalogos.tgrado_academico.asi_detalle from catalogos.tgrado_academico where catalogos.tgrado_academico.grado_id = ce.talumno.grado_id) as grado"))
		 ->whereIn('core.tpersona.persona_id', $ids)
		 ->where('core.tcuenta.rol_id',$operador,$opcion)->get()->toArray();
           */
          $personas=$query->Join('core.tcuenta','core.tcuenta.persona_id','=','core.tpersona.persona_id')
		 ->Join('core.trol','core.trol.rol_id','=','core.tcuenta.rol_id')
		 ->leftJoin('core.tpersonal','core.tpersonal.persona_id','=','core.tpersona.persona_id')
		 ->leftJoin('ce.talumno','ce.talumno.persona_id','=','core.tpersona.persona_id')
		 ->select(\DB::raw('concat(core.tpersona.per_nombre,\' \',core.tpersona.per_primer_apellido,\' \',core.tpersona.per_segundo_apellido) as consulta')
				  ,'core.tpersona.persona_id as id','core.trol.rol_nombre as rol','core.trol.rol_id','core.tpersona.per_rfc as rfc','core.tpersona.per_foto as foto'
				  ,\DB::raw("(select catalogos.tpuesto.puesto_nombre from catalogos.tpuesto where catalogos.tpuesto.puesto_id = core.tpersonal.puesto_id) as puesto")
				  ,\DB::raw("(select catalogos.tcurso.cur_nombre from catalogos.tcurso where catalogos.tcurso.curso_id = ce.talumno.curso_id) as curso")
				  ,\DB::raw("(select ce.tgrupos.grupo_nombre from ce.tgrupos where ce.tgrupos.grupo_id = ce.talumno.grupo_id) as grupo")
				  ,\DB::raw("(select catalogos.tgrado_academico.grado_detalle from catalogos.tgrado_academico where catalogos.tgrado_academico.grado_id = ce.talumno.grado_id) as grado"))
		 ->whereIn('core.tpersona.persona_id', $ids)
		 ->where('core.tcuenta.rol_id',$operador,$opcion)->get()->toArray();

           return $personas;
     }
     /*
      *Para el control de accesos en la bitacora
      **/
     public function scopeListadoPersonas($query, $page, $rows, $search, $draw)
     {
          $personas = Persona::
               join('core.tcuenta','core.tcuenta.persona_id','=','core.tpersona.persona_id')
               ->join('core.trol', 'core.trol.rol_id', '=', 'core.tcuenta.rol_id')
               ->select('core.tpersona.persona_id',
                          \DB::raw('concat(core.tpersona.per_nombre,\' \',core.tpersona.per_primer_apellido,\' \',core.tpersona.per_segundo_apellido) as nombre'),
                          'core.trol.rol_nombre'
                          )
               ->where(\DB::raw('concat(core.tpersona.per_nombre,\' \',core.tpersona.per_primer_apellido,\' \',core.tpersona.per_segundo_apellido)'),'ILIKE','%'.$search['value'].'%')
               ->orderBy('persona_id', 'desc')
               ->limit($rows)->offset($page)
               ->get()->toArray();
          $total = Persona::count();
        $datos['data']=(sizeof($personas) > 0)?$personas:array();
        $datos['recordsTotal'] = $total;
	$datos['search'] = $search['value'];
		$datos['draw'] = (int)$draw;
		$datos['recordsFiltered'] = $total;
          return $datos;
     }

     public function scopeGetDatosNombre($query, $nombre){
         $datos = $query
         ->select('persona_id','per_segundo_apellido','per_primer_apellido', 'per_nombre',
                  \DB::raw("CONCAT(per_nombre, ' ', per_primer_apellido, ' ', per_segundo_apellido) as nombre"))
         ->where(\DB::raw("CONCAT(per_nombre, ' ', per_primer_apellido, ' ', per_segundo_apellido)")
                 ,'ILIKE',"%".$nombre."%"
                 )
         ->get()->toArray();
         $datos=(sizeof($datos) > 0)?$datos:array();
         return $datos;
      }
      public function scopeGetDatosId($query, $id){
          $datos = $query
          ->select('persona_id','per_segundo_apellido','per_primer_apellido', 'per_nombre',
                   \DB::raw("CONCAT(per_nombre, ' ', per_primer_apellido, ' ', per_segundo_apellido) as nombre"))
          ->where('persona_id',$id)
          ->get()->toArray();
          $datos=(sizeof($datos) > 0)?$datos:array();
          return $datos;
       }

     // Funciones para Perfil
     public function scopeGetPerfil($query,$id){
         $datos = $query->leftJoin('core.tcuenta','core.tcuenta.persona_id','=','core.tpersona.persona_id')
          ->leftJoin('core.trol','core.trol.rol_id','=','core.tcuenta.rol_id')
         ->select('core.tpersona.persona_id','core.tpersona.edocivil_id','core.tpersona.per_nombre','core.tpersona.per_primer_apellido','core.tpersona.per_segundo_apellido'
                  ,'core.tpersona.per_rfc','core.tpersona.per_curp','core.tpersona.per_telefono','core.tpersona.per_celular','core.tpersona.per_email','core.tpersona.per_calle','core.tpersona.per_colonia'
                  ,'core.tpersona.per_domicilio_cp','core.tpersona.per_sexo','core.tpersona.per_foto','core.tpersona.per_alias'
                  ,\DB::raw("to_char(core.tpersona.per_fecha_nacimiento, 'DD/MM/YYYY') as per_fecha_nacimiento")
                  ,'core.tpersona.estado_id','core.tpersona.municipio_id','core.tpersona.reg_alimenticio'
                  ,'core.tpersona.localidad_id','core.tpersona.per_lugar_nacimiento','core.trol.rol_id'
                  ,'core.trol.rol_nombre')
         ->where('core.tpersona.persona_id','=',$id)
         ->get()->toArray();
         $datos=(sizeof($datos) > 0)?$datos:array();
         return $datos;
     }

         public function scopeFOTO($query,$id){
         $datos = $query
         ->select('persona_id','per_foto')
         ->where('persona_id', '=', $id)->get()->toArray();
        $datos=(sizeof($datos) > 0)?$datos:array(array());
         return $datos;

     }
     
        public function scopePerfil($query,$id){
          $datos = $query
         ->select('persona_id','per_nombre','per_primer_apellido','per_segundo_apellido')
         ->where('persona_id', '=', $id)
         ->get()->toArray();
        $datos=(sizeof($datos) > 0)?$datos:array(array());
         return $datos;
     }
     
     public function scopeDatosAlumno($query, $id){
          $datos=$query
          ->join('core.tcuenta','core.tcuenta.persona_id','=','core.tpersona.persona_id')
          ->join('core.trol','core.trol.rol_id','=','core.tcuenta.rol_id')
          ->join('ce.talumno','ce.talumno.persona_id','=','core.tpersona.persona_id')
          ->select('per_nombre','per_primer_apellido','per_segundo_apellido',
                   'core.trol.rol_nombre','ce.talumno.matricula')
          ->where('core.tpersona.persona_id',$id)
          ->get()->toArray();
          return $datos;
     }
     
     public function scopeClientesJSON($query,$page, $rows, $search, $draw){
          $data=$query
               ->join('core.tcuenta','core.tcuenta.persona_id','=','core.tpersona.persona_id')
               ->leftjoin('core.tpersona_datosfacturacion','core.tpersona_datosfacturacion.persona_id','=','core.tpersona.persona_id')
               ->select('core.tpersona.persona_id as editar','core.tpersona_datosfacturacion.cliente_rfc as rfc'
                        ,'core.tpersona_datosfacturacion.tipo_persona as persona_tipo','core.tpersona_datosfacturacion.email as correo'
               ,\DB::raw('concat(per_nombre,\' \',per_primer_apellido,\' \',per_segundo_apellido) as nombre'));
               
          $data=$data->where('core.tcuenta.rol_id',4)
               ->where(\DB::raw('concat(core.tpersona.per_primer_apellido,\' \',core.tpersona.per_segundo_apellido,\' \',core.tpersona.per_nombre)'), 'ILIKE','%'.$search['value'].'%')
               ->orderBy('core.tpersona.persona_id','asc')
               ->limit($rows)->offset($page)->get()->toArray();
          $totales=Persona::join('core.tcuenta','core.tcuenta.persona_id','=','core.tpersona.persona_id')
                    ->select(\DB::raw('count(core.tpersona.persona_id) as total'))
                    ->where('core.tcuenta.rol_id',4)
                    ->get()->toArray();
		$datos['data']=(sizeof($data) > 0)?$data:array();
          $datos['recordsTotal']=(isset($totales[0]['total']))?$totales[0]['total']:0;
		$datos['search'] = $search['value'];
		$datos['draw'] = (int)$draw;
		$datos['recordsFiltered'] = (isset($totales[0]['total']))?$totales[0]['total']:0;
          
          return $datos;
     }
     
     public function scopeBuscarClie($query,$buscar)
     {
       $datos = $query
       ->select('persona_id',\DB::raw('concat(per_nombre,\' \',per_primer_apellido,\' \',per_segundo_apellido) as nombre'))
       ->where(\DB::raw('concat(per_nombre,\' \',per_primer_apellido,\' \',per_segundo_apellido)'),'ILIKE',"%".$buscar."%")
       ->orderBy(\DB::raw('concat(per_nombre,\' \',per_primer_apellido,\' \',per_segundo_apellido)'),'Asc')
       ->get()->toArray();
       
       $datos=(is_object($datos)>0)?array():$datos;
       return $datos;
     }
     
     public function scopeSeleccionarClie($query,$id)
     {
       $datos = $query->leftJoin('core.tpersona_datosfacturacion','core.tpersona_datosfacturacion.persona_id','core.tpersona.persona_id', 'left outer')
       ->select('core.tpersona.persona_id',\DB::raw('concat(core.tpersona.per_nombre,\' \',core.tpersona.per_primer_apellido,\' \',core.tpersona.per_segundo_apellido) as nombre'),
               'core.tpersona.per_email','core.tpersona.per_telefono','core.tpersona.estado_id','core.tpersona.municipio_id','core.tpersona.localidad_id','core.tpersona.per_calle',
               'core.tpersona.per_colonia','core.tpersona.per_num','core.tpersona.per_domicilio_cp',
               \DB::raw('core.tpersona_datosfacturacion.cliente_nombre as fac_nombre'),
               \DB::raw('core.tpersona_datosfacturacion.cliente_apellidop as fac_apellido1'),
               \DB::raw('core.tpersona_datosfacturacion.cliente_apellidom as fac_apellido2'),
               \DB::raw('core.tpersona_datosfacturacion.tipo_persona as fac_tipo_persona'),
               \DB::raw('core.tpersona_datosfacturacion.cliente_rfc as fac_rfc'),
               \DB::raw('core.tpersona_datosfacturacion.fecha_nacimiento as fac_fecha_nacimiento'),
               \DB::raw('core.tpersona_datosfacturacion.pais_nacimiento as fac_pais'),
               \DB::raw('core.tpersona_datosfacturacion.estado_nacimiento as fac_estado'),
               \DB::raw('core.tpersona_datosfacturacion.estado_civil as fac_edocivil'),
               \DB::raw('core.tpersona_datosfacturacion.cliente_rfc as fac_rfc'),
               \DB::raw('core.tpersona_datosfacturacion.cliente_curp as fac_curp'),
               
               \DB::raw('core.tpersona_datosfacturacion.pmoral_rfc as fac_mrfc'),
               \DB::raw('core.tpersona_datosfacturacion.razon_social as fac_razon_social'),
               \DB::raw('core.tpersona_datosfacturacion.fecha_constitucion as fac_fechaconstitucion'),
               \DB::raw('core.tpersona_datosfacturacion.conyugue_nombre as fac_conyugue_nombre'),
               \DB::raw('core.tpersona_datosfacturacion.conyugue_apellidop as fac_conyugue_apellido1'),
               \DB::raw('core.tpersona_datosfacturacion.conyugue_apellidom as fac_conyugue_apellido2'),
               \DB::raw('core.tpersona_datosfacturacion.no as fac_cliente_calle_no'),
               \DB::raw('core.tpersona_datosfacturacion.no_interior as fac_nointerior'),
               \DB::raw('core.tpersona_datosfacturacion.colonia as fac_colonia'),
               \DB::raw('core.tpersona_datosfacturacion.referencia as fac_referencia'),
               \DB::raw('core.tpersona_datosfacturacion.cp as fac_cp'),
               \DB::raw('core.tpersona_datosfacturacion.no as fac_cliente_calle_no'),
               \DB::raw('core.tpersona_datosfacturacion.lada as fac_lada'),
               \DB::raw('core.tpersona_datosfacturacion.telefono as fac_telefono'),
               \DB::raw('core.tpersona_datosfacturacion.fax as fac_fax'),
               \DB::raw('core.tpersona_datosfacturacion.contacto_nombre'),
               \DB::raw('core.tpersona_datosfacturacion.email as fac_email'), 
               \DB::raw('core.tpersona_datosfacturacion.sat_pais_id as fac_pais'),
               \DB::raw('core.tpersona_datosfacturacion.identidad_tributaria as fac_identidad'),
               'core.tpersona_datosfacturacion.facturacion_id as tpersona_datos_facturacion',
               'core.tpersona_datosfacturacion.direccion_estado as fac_estado_id',
               'core.tpersona_datosfacturacion.direccion_municipio as fac_municipio_id',
               'core.tpersona_datosfacturacion.direccion_localidad as fac_localidad_id',
               'core.tpersona_datosfacturacion.cliente_calle as fac_cliente_calle',
               \DB::raw('(SELECT tipo_cliente_id FROM core.tcuenta WHERE core.tcuenta.persona_id = core.tpersona.persona_id LIMIT 1) as tipo_cliente_id')
               )
       ->where('core.tpersona.persona_id','=',$id)
       ->get()->toArray();     
       $datos=(is_object($datos)>0)?array():$datos;
       return $datos;
     }
}