<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Evidencia extends Model
{
    protected $table = 'evidencias.fotos';
    protected $primaryKey = 'id';
    
	public function scopeEditar($query,$id){
		$datos=$query
		->select('id','ruta','fecha','persona_id','descripcion',
					'categoria','nombre')
		->where('id','=',$id)
		->get()->toArray();
		$datos=is_array($datos)?$datos:array();
		return $datos;
	}
	
    public function scopeGuardar($query,$fecha,$persona_id,$doc_id,
                                 $categoria,$descipcion,$nombre,$ext,$ruta){
        if($doc_id==0){
            $documento = New Evidencias();
        }else{
            $documento = Evidencias::find($doc_id);
        }
        
        $documento->fecha=$fecha;
		$documento->persona_id=$persona_id;
        $documento->descripcion=$descripcion;
		$documento->categoria=$categoria;
        $documento->nombre=$nombre;
        $documento->ext=$ext;
        $documento->ruta=$ruta;
		
        $documento->save();
    }    
}