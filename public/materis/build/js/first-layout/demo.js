$(document).ready(function() {

    // User Profile
    // --------------------------------------------------

    $('#user-profile').easyPieChart({
        barColor: '#25293D',
        trackColor: false,
        scaleColor: false,
        scaleLength: 0,
        lineCap: 'round',
        lineWidth: 3,
        size: 100,
        animate: {
            duration: 2000,
            enabled: true
        }
    });

});