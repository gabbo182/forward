$(document).ready(function() {

    // jQuery Minicolors
    // --------------------------------------------------

    $('#hue-minicolors').minicolors({
        theme: 'bootstrap'
    });

    $('#saturation-minicolors').minicolors({
        control: 'saturation',
        theme: 'bootstrap'
    });

    $('#brightness-minicolors').minicolors({
        control: 'brightness',
        theme: 'bootstrap'
    });

    $('#wheel-minicolors').minicolors({
        control: 'wheel',
        theme: 'bootstrap'
    });

    $('#text-minicolors').minicolors({
        theme: 'bootstrap'
    });

    $('#hidden-minicolors').minicolors({
        theme: 'bootstrap'
    });

    $('#inline-minicolors').minicolors({
        inline: true,
        theme: 'bootstrap'
    });

    $('#bottom-left-minicolors').minicolors({
        theme: 'bootstrap'
    });

    $('#bottom-right-minicolors').minicolors({
        position: 'bottom right',
        theme: 'bootstrap'
    });

    $('#top-left-minicolors').minicolors({
        position: 'top left',
        theme: 'bootstrap'
    });

    $('#top-right-minicolors').minicolors({
        position: 'top right',
        theme: 'bootstrap'
    });

    $('#rgb-minicolors').minicolors({
        format: 'rgb',
        theme: 'bootstrap'
    });

    $('#rgba-minicolors').minicolors({
        format: 'rgb',
        opacity: 0.8,
        theme: 'bootstrap'
    });

    $('#opacity-minicolors').minicolors({
        opacity: true,
        theme: 'bootstrap'
    });

    $('#keywords-minicolors').minicolors({
        keywords: 'transparent',
        theme: 'bootstrap'
    });

    $('#default-value-minicolors').minicolors({
        defaultValue: '#2196f3',
        theme: 'bootstrap'
    });

    $('#letter-case-minicolors').minicolors({
        letterCase: 'uppercase',
        theme: 'bootstrap'
    });

    $('#input-group-minicolors').minicolors({
        theme: 'bootstrap'
    });

    $('#more-input-group-minicolors').minicolors({
        theme: 'bootstrap'
    });

});