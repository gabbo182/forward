$(document).ready(function() {

    // World Map
    // --------------------------------------------------

    var dataEarnings = {
        'AU': 12190,
        'AR': 3510,
        'BR': 2023,
        'CA': 1563,
        'CN': 5745,
        'FR': 2555,
        'DE': 3305,
        'JP': 5390,
        'RU': 2476,
        'US': 14624
    };
    var dataMapMarker = [{
        latLng: [41.90, 12.45],
        name: 'Vatican City',
        earnings: '500'
    }, {
        latLng: [43.73, 7.41],
        name: 'Monaco',
        earnings: '32'
    }, {
        latLng: [-0.52, 166.93],
        name: 'Nauru',
        earnings: '432'
    }, {
        latLng: [-8.51, 179.21],
        name: 'Tuvalu',
        earnings: '321'
    }, {
        latLng: [43.93, 12.46],
        name: 'San Marino',
        earnings: '510'
    }, {
        latLng: [47.14, 9.52],
        name: 'Liechtenstein',
        earnings: '130'
    }, {
        latLng: [7.11, 171.06],
        name: 'Marshall Islands',
        earnings: '234'
    }, {
        latLng: [17.3, -62.73],
        name: 'Saint Kitts and Nevis',
        earnings: '329'
    }, {
        latLng: [3.2, 73.22],
        name: 'Maldives',
        earnings: '120'
    }, {
        latLng: [35.88, 14.5],
        name: 'Malta',
        earnings: '435'
    }, {
        latLng: [12.05, -61.75],
        name: 'Grenada',
        earnings: '321'
    }, {
        latLng: [13.16, -61.23],
        name: 'Saint Vincent and the Grenadines',
        earnings: '110'
    }, {
        latLng: [13.16, -59.55],
        name: 'Barbados',
        earnings: '90'
    }, {
        latLng: [17.11, -61.85],
        name: 'Antigua and Barbuda',
        earnings: '230'
    }, {
        latLng: [-4.61, 55.45],
        name: 'Seychelles',
        earnings: '200'
    }, {
        latLng: [7.35, 134.46],
        name: 'Palau',
        earnings: '320'
    }, {
        latLng: [42.5, 1.51],
        name: 'Andorra',
        earnings: '123'
    }, {
        latLng: [14.01, -60.98],
        name: 'Saint Lucia',
        earnings: '500'
    }, {
        latLng: [6.91, 158.18],
        name: 'Federated States of Micronesia',
        earnings: '310'
    }, {
        latLng: [1.3, 103.8],
        name: 'Singapore',
        earnings: '23'
    }, {
        latLng: [1.46, 173.03],
        name: 'Kiribati',
        earnings: '58'
    }, {
        latLng: [-21.13, -175.2],
        name: 'Tonga',
        earnings: '90'
    }, {
        latLng: [15.3, -61.38],
        name: 'Dominica',
        earnings: '239'
    }, {
        latLng: [-20.2, 57.5],
        name: 'Mauritius',
        earnings: '100'
    }, {
        latLng: [26.02, 50.55],
        name: 'Bahrain',
        earnings: '225'
    }, {
        latLng: [0.33, 6.73],
        name: 'São Tomé and Príncipe',
        earnings: '150'
    }];
    $('#world-map').vectorMap({
        map: 'world_mill',
        backgroundColor: 'transparent',
        zoomOnScroll: false,
        zoomButtons: false,
        focusOn: {
            x: 1,
            y: 1,
            scale: 3
        },
        regionStyle: {
            initial: {
                fill: '#25293D'
            }
        },
        markers: dataMapMarker,
        markerStyle: {
            initial: {
                fill: '#F44336',
                stroke: '#F44336',
                'stroke-width': 10,
                'stroke-opacity': 0.2,
                r: 5
            },
            hover: {
                stroke: '#F44336',
                'stroke-width': 10,
                'stroke-opacity': 0.5,
                cursor: 'pointer'
            }
        },
        onRegionTipShow: function(e, el, code) {
            if (dataEarnings.hasOwnProperty(code)) {
                el.html(el.html() + ': $' + dataEarnings[code].toFixed(0).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ','));
            }
        },
        onMarkerTipShow: function(e, el, code) {
            el.html(el.html() + ': $' + dataMapMarker[code].earnings);
        }
    });

    // ThemeForest
    // --------------------------------------------------

    $('#themeforest').easyPieChart({
        barColor: '#25293D',
        trackColor: '#E6E6E6',
        scaleColor: false,
        scaleLength: 0,
        lineCap: 'square',
        lineWidth: 10,
        size: 80,
        animate: {
            duration: 2000,
            enabled: true
        },
        onStep: function(from, to, percent) {
            $(this.el).find('.percent').text(Math.round(percent) + '%');
        }
    });

    // GraphicRiver
    // --------------------------------------------------

    $('#graphicriver').easyPieChart({
        barColor: '#3F51B5',
        trackColor: '#E6E6E6',
        scaleColor: false,
        scaleLength: 0,
        lineCap: 'square',
        lineWidth: 10,
        size: 80,
        animate: {
            duration: 2000,
            enabled: true
        },
        onStep: function(from, to, percent) {
            $(this.el).find('.percent').text(Math.round(percent) + '%');
        }
    });

    // Envato Market
    // --------------------------------------------------

    var dataThemeForest = [
        [4400, 0],
        [3700, 1],
        [3500, 2],
        [3600, 3],
        [4600, 4]
    ];
    var dataGraphicRiver = [
        [3200, 0],
        [2400, 1],
        [4000, 2],
        [3800, 3],
        [2900, 4]
    ];
    var dataOtherMarket = [
        [2500, 0],
        [3300, 1],
        [2600, 2],
        [3600, 3],
        [2900, 4]
    ];
    var yticksMarket = [
        [0, 'Jan'],
        [1, 'Feb'],
        [2, 'Mar'],
        [3, 'Apr'],
        [4, 'May']
    ];
    var datasetMarket = [{
        label: 'ThemeForest',
        data: dataThemeForest,
        color: '#25293D'
    }, {
        label: 'GraphicRiver',
        data: dataGraphicRiver,
        color: '#3F51B5'
    }, {
        label: 'Other',
        data: dataOtherMarket,
        color: '#E6E6E6'
    }];
    var optionsMarket = {
        series: {
            stack: true,
            bars: {
                show: true,
                fill: 1,
                align: 'center',
                lineWidth: 0,
                horizontal: true,
                barWidth: 0.3
            }
        },
        grid: {
            borderWidth: 0,
            hoverable: true,
            labelMargin: 20
        },
        yaxis: {
            ticks: yticksMarket,
            tickLength: 0,
            font: {
                color: '#78909C',
                size: 12
            }
        },
        xaxis: {
            tickLength: 0,
            font: {
                color: '#78909C',
                size: 12
            },
            tickFormatter: function(val, axis) {
                if (val > 0) {
                    return (val / 1000).toFixed(axis.tickDecimals) + 'K';
                } else {
                    return (val / 1000).toFixed(axis.tickDecimals);
                }
            }
        },
        tooltip: {
            show: false
        },
        legend: {
            show: false
        }
    };
    $.plot($('#flot-market'), datasetMarket, optionsMarket);
    $('#flot-market').bind('plothover', function(event, pos, item) {
        if (item) {
            $('.flotTip').text(item.series.label + ': $' + (item.datapoint[0] - item.datapoint[2]).toFixed(0).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',')).css({
                top: item.pageY + 15,
                left: item.pageX + 10
            }).show();
        } else {
            $('.flotTip').hide();
        }
    });

    // Latest Projects
    // --------------------------------------------------

    $('#esp-umega').easyPieChart({
        barColor: '#3F51B5',
        trackColor: false,
        scaleColor: false,
        scaleLength: 0,
        lineCap: 'round',
        lineWidth: 15,
        size: 220,
        rotate: 180,
        animate: {
            duration: 2000,
            enabled: true
        }
    });

    $('#esp-materis').easyPieChart({
        barColor: '#E91E63',
        trackColor: false,
        scaleColor: false,
        scaleLength: 0,
        lineCap: 'round',
        lineWidth: 15,
        size: 160,
        rotate: 180,
        animate: {
            duration: 2000,
            enabled: true
        }
    });

    $('#esp-sigma').easyPieChart({
        barColor: '#673AB7',
        trackColor: false,
        scaleColor: false,
        scaleLength: 0,
        lineCap: 'round',
        lineWidth: 15,
        size: 100,
        rotate: 180,
        animate: {
            duration: 2000,
            enabled: true
        }
    });

    // Visitors Analytics
    // --------------------------------------------------

    var dataReturningVisitor = [
        [0, 65070],
        [1, 11025],
        [2, 41701],
        [3, 49549],
        [4, 88760],
        [5, 56477],
        [6, 25801]
    ];
    var dataNewVisitor = [
        [0, 15070],
        [1, 50250],
        [2, 22062],
        [3, 82118],
        [4, 23359],
        [5, 40878],
        [6, 36462]
    ];
    var xticksVisitor = [
        [0, 'Mon'],
        [1, 'Tue'],
        [2, 'Wed'],
        [3, 'Thu'],
        [4, 'Fri'],
        [5, 'Sat'],
        [6, 'Sun']
    ];
    var datasetVisitor = [{
        label: 'Returning visitors',
        data: dataReturningVisitor,
        color: '#25293D'
    }, {
        label: 'New visitors',
        data: dataNewVisitor,
        color: '#3F51B5'
    }];
    var optionsVisitor = {
        series: {
            lines: {
                show: true,
            },
            points: {
                show: true,
                radius: 6
            },
            shadowSize: 5
        },
        grid: {
            borderWidth: 0,
            hoverable: true,
            labelMargin: 20
        },
        xaxis: {
            tickColor: '#ECEFF1',
            ticks: xticksVisitor,
            font: {
                color: '#78909C',
                size: 12
            }
        },
        yaxis: {
            tickLength: 0,
            tickSize: 20000,
            font: {
                color: '#78909C',
                size: 12
            },
            tickFormatter: function(val, axis) {
                if (val > 0) {
                    return (val / 1000).toFixed(axis.tickDecimals) + 'K';
                } else {
                    return (val / 1000).toFixed(axis.tickDecimals);
                }
            }
        },
        tooltip: {
            show: false
        },
        legend: {
            show: true,
            position: 'ne',
            noColumns: 1,
            labelBoxBorderColor: '#FFF',
            margin: [10, 0]
        }
    };
    $.plot($('#flot-visitor'), datasetVisitor, optionsVisitor);
    $('#flot-visitor').bind('plothover', function(event, pos, item) {
        if (item) {
            $('.flotTip').text(item.series.label + ': ' + item.datapoint[1].toFixed(0).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',')).css({
                top: item.pageY + 20,
                left: item.pageX - 50
            }).show();
        } else {
            $('.flotTip').hide();
        }
    });

    // Bootstrap Date Range Picker
    // --------------------------------------------------

    $('#daterangepicker').daterangepicker({
        ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract('days', 1), moment().subtract('days', 1)],
            'Last 7 Days': [moment().subtract('days', 6), moment()],
            'Last 30 Days': [moment().subtract('days', 29), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')]
        },
        opens: 'left',
        startDate: moment().subtract(29, 'days'),
        endDate: moment(),
        buttonClasses: 'btn',
        applyClass: 'btn-primary mr-1',
        cancelClass: 'btn-outline btn-secondary'
    }, function(start, end, label) {
        $('#daterangepicker span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
    });
    $('#daterangepicker span').html(moment().subtract(29, 'days').format('MMMM D, YYYY') + ' - ' + moment().format('MMMM D, YYYY'));

    // Weather Carousel
    // --------------------------------------------------

    $('#weather-carousel').slick({
        slidesToShow: 3,
        slidesToScroll: 2,
        dots: true,
        arrows: false,
        autoplay: true,
        autoplaySpeed: 3000
    });

    // New Orders by Device
    // --------------------------------------------------

    var datasetOrder = [{
        label: 'Laptop',
        data: 24325,
        color: '#673AB7'
    }, {
        label: 'Tablet',
        data: 16257,
        color: '#009688'
    }, {
        label: 'Mobile',
        data: 10314,
        color: '#2196F3'
    }, {
        label: 'Other',
        data: 14930,
        color: '#E91E63'
    }];
    var optionsOrder = {
        series: {
            pie: {
                show: true,
                stroke: {
                    width: 0
                },
                innerRadius: 0.5,
                label: {
                    show: false
                },
                highlight: {
                    opacity: 0.3
                }
            }
        },
        grid: {
            hoverable: true
        },
        tooltip: {
            show: true,
            content: '%s: %p.0%',
            defaultTheme: false
        },
        legend: {
            show: false
        }
    };
    $.plot($('#flot-order'), datasetOrder, optionsOrder);

    // New Comments
    // --------------------------------------------------

    var dataComment = [
        [0, 15],
        [1, 162],
        [2, 82],
        [3, 252],
        [4, 133],
        [5, 308],
        [6, 206],
        [7, 408]
    ];
    var datasetComment = [{
        label: 'New Comments',
        data: dataComment,
        color: '#F44336',
        lines: {
            show: true
        },
        curvedLines: {
            apply: true,
            monotonicFit: true
        }
    }, {
        data: dataComment,
        color: '#F44336',
        lines: {
            show: false
        },
        points: {
            show: true
        }
    }];
    var optionsComment = {
        series: {
            curvedLines: {
                active: true
            },
            shadowSize: 0
        },
        grid: {
            borderWidth: 0
        },
        xaxis: {
            show: false
        },
        yaxis: {
            show: false
        },
        tooltip: {
            show: false
        },
        legend: {
            show: false
        }
    };
    $.plot($('#flot-comment'), datasetComment, optionsComment);

    // New Feedbacks
    // --------------------------------------------------

    var dataFeedback = [
        [0, 15],
        [1, 162],
        [2, 82],
        [3, 252],
        [4, 133],
        [5, 308],
        [6, 206],
        [7, 408]
    ];
    var datasetFeedback = [{
        label: 'New Feedbacks',
        data: dataFeedback,
        color: '#25293D',
        lines: {
            show: true
        },
        curvedLines: {
            apply: true,
            monotonicFit: true
        }
    }, {
        data: dataFeedback,
        color: '#25293D',
        lines: {
            show: false
        },
        points: {
            show: true
        }
    }];
    var optionsFeedback = {
        series: {
            curvedLines: {
                active: true
            },
            shadowSize: 0
        },
        grid: {
            borderWidth: 0
        },
        xaxis: {
            show: false
        },
        yaxis: {
            show: false
        },
        tooltip: {
            show: false
        },
        legend: {
            show: false
        }
    };
    $.plot($('#flot-feedback'), datasetFeedback, optionsFeedback);

    //  Latest Orders
    // --------------------------------------------------

    var table = $('#order-table').DataTable({
        lengthChange: false,
        paging: false,
        info: false,
        colReorder: true,
        language: {
            'search': '',
            'searchPlaceholder': 'Search records'
        },
        buttons: {
            buttons: [{
                extend: 'copy',
                className: 'btn-inverse'
            }, {
                extend: 'excel',
                className: 'btn-inverse'
            }, {
                extend: 'pdf',
                className: 'btn-inverse'
            }, {
                extend: 'print',
                className: 'btn-inverse'
            }]
        }
    });
    table.buttons().container().appendTo('#order-table_wrapper .col-md-6:eq(0)');

    // Server Load
    // --------------------------------------------------

    var dataServer = [
        [0, 567],
        [1, 174],
        [2, 467],
        [3, 829],
        [4, 489],
        [5, 360],
        [6, 380],
        [7, 907],
        [8, 549],
        [9, 765],
        [10, 479],
        [11, 783],
        [12, 357],
        [13, 985],
        [14, 370]
    ];
    var datasetServer = [{
        label: 'Server load',
        data: dataServer,
        color: '#FFF',
    }];
    var optionsServer = {
        series: {
            bars: {
                show: true,
                fill: 0.2,
                align: 'center',
                barWidth: 0.5,
                lineWidth: 0
            }
        },
        grid: {
            borderWidth: 0
        },
        xaxis: {
            show: false
        },
        yaxis: {
            show: false
        },
        tooltip: {
            show: false
        },
        legend: {
            show: false
        }
    };
    $.plot($('#flot-server'), datasetServer, optionsServer);

    // Current Website Visitors
    // --------------------------------------------------

    var dataCurrentVisitor = [
        [0, 764],
        [1, 567],
        [2, 326],
        [3, 964],
        [4, 769],
        [5, 655],
        [6, 453],
        [7, 567],
        [8, 876],
        [9, 645],
        [10, 348],
        [11, 854],
        [12, 580],
        [13, 876],
        [14, 1190]
    ];
    var datasetCurrentVisitor = [{
        label: 'Current Website Visitors',
        data: dataCurrentVisitor,
        color: '#FFF',
        lines: {
            show: true,
            fill: 0.2,
            lineWidth: 0
        }
    }];
    var optionsCurrentVisitor = {
        series: {
            curvedLines: {
                apply: true,
                monotonicFit: true,
                active: true
            },
            shadowSize: 0
        },
        grid: {
            borderWidth: 0
        },
        xaxis: {
            show: false
        },
        yaxis: {
            show: false
        },
        tooltip: {
            show: false
        },
        legend: {
            show: false
        }
    };
    $.plot($('#flot-current-visitor'), datasetCurrentVisitor, optionsCurrentVisitor);

    $('<div class=\'flotTip\'></div>').appendTo('body').css({
        'position': 'absolute',
        'display': 'none'
    });

});