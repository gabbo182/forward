$(document).ready(function() {

    $('#form-vertical').steps({
        headerTag: 'h4',
        bodyTag: 'fieldset',
        transitionEffect: 'slide',
        stepsOrientation: 'vertical'
    });

});