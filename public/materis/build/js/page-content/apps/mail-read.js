$(document).ready(function() {

    $('#mail-compose').summernote({
        fontNames: ['Heebo', 'Kalam', 'Arial', 'Helvetica', 'Tahoma', 'Times New Roman', 'Verdana'],
        height: 200
    });

    $('.popup-gallery').magnificPopup({
        delegate: 'a',
        type: 'image',
        tLoading: 'Loading image #%curr%...',
        gallery: {
            enabled: true,
            navigateByImgClick: true,
            preload: [0, 1]
        },
        image: {
            tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
            titleSrc: function(item) {
                return item.el.attr('title');
            }
        }
    });

});