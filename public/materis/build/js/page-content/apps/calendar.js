$(document).ready(function() {

    // External Events
    // --------------------------------------------------

    $('.draggable li').each(function() {
        $(this).data('event', {
            title: $.trim($(this).text()),
            stick: true
        });
        $(this).draggable({
            zIndex: 9999,
            revert: true,
            revertDuration: 0,
            cursor: 'crosshair'
        });
    });
    $('#fullCalendar').fullCalendar({
        header: {
            left: 'prev,next',
            center: 'title',
            right: 'month,agendaWeek,agendaDay'
        },
        buttonIcons: {
            prev: ' zmdi zmdi-chevron-left',
            next: ' zmdi zmdi-chevron-right'
        },
        defaultDate: '2017-03-15',
        editable: true,
        droppable: true,
        selectable: true,
        select: function(start, end, allDay) {
            $('#start').val(moment(start).format('YYYY/MM/DD hh:mm a'));
            $('#end').val(moment(end).format('YYYY/MM/DD hh:mm a'));
            $('#inputTitleEvent').val('');
            $('#addNewEvent').modal('show');
        },
        eventColor: '#25293D',
        eventTextColor: '#FFF',
        eventLimit: true,
        events: [{
            title: 'All Day Event',
            start: '2017-03-18',
            color: '#3F51B5',
            textColor: '#FFF'
        }, {
            title: 'Long Event',
            start: '2017-03-07',
            end: '2017-03-10',
            color: '#2196F3',
            textColor: '#FFF'
        }, {
            title: 'Repeating Event',
            start: '2017-03-28T16:00:00',
            color: '#673AB7',
            textColor: '#FFF'
        }, {
            title: 'Repeating Event',
            start: '2017-03-16T16:00:00',
            color: '#009688',
            textColor: '#FFF'
        }, {
            title: 'Conference',
            start: '2017-03-11',
            end: '2017-03-13',
            color: '#8BC34A',
            textColor: '#FFF'
        }, {
            title: 'Meeting',
            start: '2017-03-12T10:30:00',
            end: '2017-03-12T12:30:00',
            color: '#009688',
            textColor: '#FFF'
        }, {
            title: 'Lunch',
            start: '2017-03-12T12:00:00',
            color: '#E91E63',
            textColor: '#FFF'
        }, {
            title: 'Meeting',
            start: '2017-03-12T14:30:00',
            color: '#009688',
            textColor: '#FFF'
        }, {
            title: 'Happy Hour',
            start: '2017-03-12T17:30:00',
            color: '#607D8B',
            textColor: '#FFF'
        }, {
            title: 'Dinner',
            start: '2017-03-12T20:00:00',
            color: '#3F51B5',
            textColor: '#FFF'
        }, {
            title: 'Birthday Party',
            start: '2017-03-13T07:00:00',
            color: '#F44336',
            textColor: '#FFF'
        }, {
            title: 'Click for Google',
            url: 'http://google.com/',
            start: '2017-03-28',
            color: '#F44336',
            textColor: '#FFF'
        }],
        drop: function() {
            if ($('#drop-remove').is(':checked')) {
                $(this).remove();
            }
        }
    });
    $('#btnAddNewEvent').on('click', function(e) {
        e.preventDefault();
        addNewEvent();
    });

    function addNewEvent() {
        $('#addNewEvent').modal('hide');
        $('#fullCalendar').fullCalendar('renderEvent', {
            title: $('#inputTitleEvent').val(),
            start: new Date($('#start').val()),
            end: new Date($('#end').val()),
            color: $('#inputBackgroundEvent').val(),
            textColor: '#FFF'
        }, true);
    }

    // jQuery Minicolors
    // --------------------------------------------------

    $('#inputBackgroundEvent').minicolors({
        theme: 'bootstrap'
    });

});