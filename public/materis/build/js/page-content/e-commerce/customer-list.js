$(document).ready(function() {

    $('#date-added').daterangepicker({
        singleDatePicker: true,
    });

    //  Customer List
    // --------------------------------------------------

    var table = $('#customer-list').DataTable({
        lengthChange: false,
        colReorder: true,
        language: {
            'search': '',
            'searchPlaceholder': 'Search records'
        },
        buttons: {
            buttons: [{
                extend: 'copy',
                className: 'btn-inverse'
            }, {
                extend: 'excel',
                className: 'btn-inverse'
            }, {
                extend: 'pdf',
                className: 'btn-inverse'
            }, {
                extend: 'print',
                className: 'btn-inverse'
            }]
        },
        order: [
            [5, 'desc']
        ],
        columnDefs: [{
            orderable: false,
            targets: [0, 6]
        }]
    });
    table.buttons().container().appendTo('#customer-list_wrapper .col-md-6:eq(0)');

});