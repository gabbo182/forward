$(document).ready(function() {

    $('#date-added, #date-modified').daterangepicker({
        singleDatePicker: true,
    });

    //  Order List
    // --------------------------------------------------

    var table = $('#order-list').DataTable({
        lengthChange: false,
        colReorder: true,
        language: {
            'search': '',
            'searchPlaceholder': 'Search records'
        },
        buttons: {
            buttons: [{
                extend: 'copy',
                className: 'btn-inverse'
            }, {
                extend: 'excel',
                className: 'btn-inverse'
            }, {
                extend: 'pdf',
                className: 'btn-inverse'
            }, {
                extend: 'print',
                className: 'btn-inverse'
            }]
        },
        order: [
            [1, 'asc']
        ],
        columnDefs: [{
            orderable: false,
            targets: [0, 7]
        }]
    });
    table.buttons().container().appendTo('#order-list_wrapper .col-md-6:eq(0)');

});