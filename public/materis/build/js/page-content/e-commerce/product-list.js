$(document).ready(function() {

    //  Product List
    // --------------------------------------------------

    var table = $('#product-list').DataTable({
        lengthChange: false,
        colReorder: true,
        language: {
            'search': '',
            'searchPlaceholder': 'Search records'
        },
        buttons: {
            buttons: [{
                extend: 'copy',
                className: 'btn-inverse'
            }, {
                extend: 'excel',
                className: 'btn-inverse'
            }, {
                extend: 'pdf',
                className: 'btn-inverse'
            }, {
                extend: 'print',
                className: 'btn-inverse'
            }]
        },
        order: [
            [2, 'asc']
        ],
        columnDefs: [{
            orderable: false,
            targets: [0, 7]
        }]
    });
    table.buttons().container().appendTo('#product-list_wrapper .col-md-6:eq(0)');

});