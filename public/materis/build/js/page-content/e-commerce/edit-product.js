$(document).ready(function() {

    // Summernote
    // --------------------------------------------------

    $('#txtDescription').summernote({
        fontNames: ['Heebo', 'Kalam', 'Arial', 'Helvetica', 'Tahoma', 'Times New Roman', 'Verdana'],
        height: 500
    });

});