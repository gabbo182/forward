$(document).ready(function() {

    $('#user-profile').easyPieChart({
        barColor: '#3F51B5',
        trackColor: false,
        scaleColor: false,
        scaleLength: 0,
        lineCap: 'round',
        lineWidth: 3,
        size: 100,
        animate: {
            duration: 2000,
            enabled: true
        }
    });

});