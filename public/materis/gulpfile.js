var gulp = require('gulp');
var browserSync = require('browser-sync').create();
var jade = require('gulp-jade');
var sass = require('gulp-sass');
var uglify = require('gulp-uglify');
var cleanCSS = require('gulp-clean-css');
var postcss = require('gulp-postcss');
var sourcemaps = require('gulp-sourcemaps');
var autoprefixer = require('autoprefixer');

gulp.task('browser-sync', function() {
    browserSync.init({
        proxy: 'localhost/Materis/first-layout'
    });
    gulp.watch(['assets/sass/*.scss', 'assets/sass/**/*.scss'], ['sass']);
    gulp.watch(['assets/jade/*.jade', 'assets/jade/**/*.jade'], ['jade']);
    gulp.watch(['assets/js/*.js', 'assets/js/**/*.js'], ['js']);
});

gulp.task('jade', function() {
    gulp.src(['assets/jade/first-layout/dashboard/index.jade'])
        .pipe(jade({
            pretty: true
        }))
        .pipe(gulp.dest('first-layout'))
        .pipe(browserSync.stream());
});

gulp.task('sass', function() {
    gulp.src(['assets/sass/first-layout/first-layout.scss'])
        .pipe(sass().on('error', sass.logError))
        //.pipe(sourcemaps.init())
        .pipe(postcss([autoprefixer()]))
        .pipe(cleanCSS())
        //.pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('build/css'))
        .pipe(browserSync.stream());
});

gulp.task('js', function() {
    gulp.src(['assets/js/first-layout/*.js'])
        .pipe(uglify())
        .pipe(gulp.dest('build/js/first-layout'))
        .pipe(browserSync.stream());
});

gulp.task('default', ['browser-sync'], function() {
    console.log('***** Materis - Bootstrap 4 Admin Template *****');
});