<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    //return redirect('/inicio'); 
    return view('welcome');
});

Route::group(['middleware' => 'auth'], function () {

    //Please do not remove this if you want adminlte:route and adminlte:link commands to works correctly.
    #adminlte_routes
});
Route::get('inicio','InicioController@inicio');
//USUARIOS
Route::get('usuarios/listado','Core\UsuariosController@listado');
Route::post('usuarios/listado/json','Core\UsuariosController@listadojson');
Route::get('usuarios/editar/{id}','Core\UsuariosController@index');
Route::post('usuarios/guardar','Core\UsuariosController@guardar');


//EVIDENCIAS
Route::get('evidencias/listado','Evidencias\EvidenciasController@listado');
Route::post('evidencias/listado/json','Evidencias\EvidenciasController@listadojson');
Route::get('evidencias/editar/{id}','Evidencias\EvidenciasController@index');
Route::post('evidencias/guardar','Evidencias\EvidenciasController@guardar');