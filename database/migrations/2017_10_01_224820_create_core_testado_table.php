<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoreTestadoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('core.testado', function (Blueprint $table) {
            $table->increments('estado_id'); 
            $table->text('cve_ent')->nullable();
            $table->text('nom_ent')->nullable();
            $table->text('nom_abr')->nullable();
            $table->text('cve_cap')->nullable();
            $table->text('nom_cap')->nullable();
            $table->text('ptot')->nullable();
            $table->text('pmas')->nullable();
            $table->text('pfem')->nullable();
            $table->text('vtot')->nullable();
            $table->timestamps();
            $table->softDeletes(); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('core.testado');
    }
}
