<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoreTlocalidadTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('core.tlocalidad', function (Blueprint $table) {
            $table->integer('estado_id');
                $table->integer('municipio_id');
                $table->integer('localidad_id');
                $table->text('cve_ent')->nullable();
                $table->text('nom_ent')->nullable();
                $table->text('nom_abr')->nullable();
                $table->text('cve_mun')->nullable();
                $table->text('nom_mu')->nullable();
                $table->text('cve_loc')->nullable();
                $table->text('nom_loc')->nullable();
                $table->text('latitud')->nullable();
                $table->text('longitud')->nullable();
                $table->text('altitud')->nullable();
                $table->text('cve_carta')->nullable();
                $table->text('ambito')->nullable();
                $table->text('ptot')->nullable();
                $table->text('pmas')->nullable();
                $table->text('pfem')->nullable();
                $table->text('vtot')->nullable();
                $table->timestamps();
                $table->softDeletes();
                $table->foreign(['municipio_id','estado_id'])->references(['municipio_id','estado_id'])->on('core.tmunicipio');
                $table->unique(array('estado_id','municipio_id','localidad_id'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('core.tlocalidad');
    }
}
