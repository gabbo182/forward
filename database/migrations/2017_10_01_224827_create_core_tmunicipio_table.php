<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoreTmunicipioTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('core.tmunicipio', function (Blueprint $table) {
            $table->integer('municipio_id');
            $table->integer('estado_id');
            $table->text('cve_ent')->nullable();
            $table->text('nom_ent')->nullable();
            $table->text('nom_abr')->nullable();
            $table->text('cve_mun');
            $table->text('nom_mun')->nullable();
            $table->text('cve_cab')->nullable();
            $table->text('nom_cab')->nullable();
            $table->text('ptot')->nullable();
            $table->text('pmas')->nullable();
            $table->text('pfem')->nullable();
            $table->text('vtot')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('estado_id')->references('estado_id')->on('core.testado');
            $table->primary(array('estado_id','municipio_id'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('core.tmunicipio');
    }
}
