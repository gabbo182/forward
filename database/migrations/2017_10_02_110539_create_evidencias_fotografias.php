<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEvidenciasFotografias extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('evidencias.fotos', function (Blueprint $table) {
            $table->increments('id');
			$table->text('nombre');
			$table->integer('persona_id');
			$table->text('categoria');
            $table->date('fecha')->nullable();
			$table->text('descripcion')->nullable();
			$table->text('ext')->nullable();
			$table->text('ruta')->nullable();
			$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('evidencias.fotos');
    }
}
