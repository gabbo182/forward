<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoreTpersonaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('core.tpersona', function (Blueprint $table) {
            $table->increments('persona_id');
			$table->text('nombre');
			$table->text('apaterno');
			$table->text('amaterno')->nullable();
			$table->text('curp')->nullable();
			$table->text('telefono')->nullable();
			$table->text('tel_celular')->nullable();
			$table->date('fecha_nacimiento')->nullable();
			$table->text('email')->nullable();
			$table->integer('sexo')->nullable();
			$table->integer('estado_id')->nullable();
			$table->integer('municipio_id')->nullable();
			$table->integer('localidad_id')->nullable();
			$table->timestamps();
			$table->foreign(['estado_id','municipio_id','localidad_id'])->references(['estado_id','municipio_id','localidad_id'])->on('core.tlocalidad');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('core.tpersona');
    }
}
