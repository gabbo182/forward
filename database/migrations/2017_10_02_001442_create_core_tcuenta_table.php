<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoreTcuentaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('core.tcuenta', function (Blueprint $table) {
            $table->increments('cuenta_id');
            $table->integer('persona_id');
            $table->integer('rol_id')->nullable();
            $table->text('usuario')->unique();
            $table->text('password');
            $table->text('email')->nullable();
            $table->text('idioma')->nullable(); 
            $table->integer('estatus')->nullable();
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('persona_id')->references('persona_id')->on('core.tpersona');
            $table->foreign('rol_id')->references('rol_id')->on('core.trol');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('core.tcuenta');
    }
}
