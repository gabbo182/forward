<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoreTrolTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('core.trol', function (Blueprint $table) {
            $table->increments('rol_id');
            $table->text('nombre')->nullable();
            $table->text('descripcion')->nullable();
            $table->text('rutainicial')->nullable();
            $table->integer('estatus')->default(1);
            $table->timestamps(); 
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('core.trol');
    }
}
