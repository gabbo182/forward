<?php

use Illuminate\Database\Seeder;

class seeders_full extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(core_testado::class);
        $this->call(core_tmunicipio::class);
        $this->call(localidad_prueba::class);
        //$this->call(core_tlocalidad::class);
        $this->call(core_trol::class);
        $this->call(core_tpersona::class);
        $this->call(core_tcuenta::class);
        $this->call(Evidencia_Categoria_Seeder::class);
    }
}
