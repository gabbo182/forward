<?php

use Illuminate\Database\Seeder;

class core_testado extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('core.testado')->insert(['cve_ent'=>'01' ,'nom_ent'=>'Aguascalientes','nom_abr'=>'Ags.', 'cve_cap'=>'0010001', 'nom_cap'=>'Aguascalientes', 'ptot'=>'1184996', 'pmas'=>'576638', 'pfem'=>'608358', 'vtot'=>'290877']); 
        \DB::table('core.testado')->insert(['cve_ent'=>'02' ,'nom_ent'=>'Baja California','nom_abr'=>'BC', 'cve_cap'=>'0020001', 'nom_cap'=>'Mexicali', 'ptot'=>'3155070', 'pmas'=>'1591610', 'pfem'=>'1563460', 'vtot'=>'870769']); 
        \DB::table('core.testado')->insert(['cve_ent'=>'03' ,'nom_ent'=>'Baja California Sur','nom_abr'=>'BCS', 'cve_cap'=>'0030001', 'nom_cap'=>'La Paz', 'ptot'=>'637026', 'pmas'=>'325433', 'pfem'=>'311593', 'vtot'=>'178271']); 
        \DB::table('core.testado')->insert(['cve_ent'=>'04' ,'nom_ent'=>'Campeche','nom_abr'=>'Camp.', 'cve_cap'=>'0020001', 'nom_cap'=>'San Francisco de Campeche', 'ptot'=>'822441', 'pmas'=>'407721', 'pfem'=>'414720', 'vtot'=>'213727']); 
        \DB::table('core.testado')->insert(['cve_ent'=>'05' ,'nom_ent'=>'Coahuila de Zaragoza','nom_abr'=>'Coah.', 'cve_cap'=>'0300001', 'nom_cap'=>'Saltillo', 'ptot'=>'2748391', 'pmas'=>'1364197', 'pfem'=>'1384194', 'vtot'=>'727613']); 
        \DB::table('core.testado')->insert(['cve_ent'=>'06' ,'nom_ent'=>'Colima','nom_abr'=>'Col.', 'cve_cap'=>'0020001', 'nom_cap'=>'Colima', 'ptot'=>'650555', 'pmas'=>'322790', 'pfem'=>'327765', 'vtot'=>'180488']); 
        \DB::table('core.testado')->insert(['cve_ent'=>'07' ,'nom_ent'=>'Chiapas','nom_abr'=>'Chis.', 'cve_cap'=>'1010001', 'nom_cap'=>'Tuxtla Gutiérrez', 'ptot'=>'4796580', 'pmas'=>'2352807', 'pfem'=>'2443773', 'vtot'=>'1091100']); 
        \DB::table('core.testado')->insert(['cve_ent'=>'08' ,'nom_ent'=>'Chihuahua','nom_abr'=>'Chih.', 'cve_cap'=>'0190001', 'nom_cap'=>'Chihuahua', 'ptot'=>'3406465', 'pmas'=>'1692545', 'pfem'=>'1713920', 'vtot'=>'944681']); 
        \DB::table('core.testado')->insert(['cve_ent'=>'09' ,'nom_ent'=>'Ciudad de México','nom_abr'=>'CDMX', 'cve_cap'=>'', 'nom_cap'=>'', 'ptot'=>'8851080', 'pmas'=>'4233783', 'pfem'=>'4617297', 'vtot'=>'2453770']); 
        \DB::table('core.testado')->insert(['cve_ent'=>'10' ,'nom_ent'=>'Durango','nom_abr'=>'Dgo.', 'cve_cap'=>'0050001', 'nom_cap'=>'Victoria de Durango', 'ptot'=>'1632934', 'pmas'=>'803890', 'pfem'=>'829044', 'vtot'=>'407784']); 
        \DB::table('core.testado')->insert(['cve_ent'=>'11' ,'nom_ent'=>'Guanajuato','nom_abr'=>'Gto.', 'cve_cap'=>'0150001', 'nom_cap'=>'Guanajuato', 'ptot'=>'5486372', 'pmas'=>'2639425', 'pfem'=>'2846947', 'vtot'=>'1276913']); 
        \DB::table('core.testado')->insert(['cve_ent'=>'12' ,'nom_ent'=>'Guerrero','nom_abr'=>'Gro.', 'cve_cap'=>'0290001', 'nom_cap'=>'Chilpancingo de los Bravo', 'ptot'=>'3388768', 'pmas'=>'1645561', 'pfem'=>'1743207', 'vtot'=>'810596']); 
        \DB::table('core.testado')->insert(['cve_ent'=>'13' ,'nom_ent'=>'Hidalgo','nom_abr'=>'Hgo.', 'cve_cap'=>'0480001', 'nom_cap'=>'Pachuca de Soto', 'ptot'=>'2665018', 'pmas'=>'1285222', 'pfem'=>'1379796', 'vtot'=>'669514']); 
        \DB::table('core.testado')->insert(['cve_ent'=>'14' ,'nom_ent'=>'Jalisco','nom_abr'=>'Jal.', 'cve_cap'=>'0390001', 'nom_cap'=>'Guadalajara', 'ptot'=>'7350682', 'pmas'=>'3600641', 'pfem'=>'3750041', 'vtot'=>'1831205']); 
        \DB::table('core.testado')->insert(['cve_ent'=>'15' ,'nom_ent'=>'México','nom_abr'=>'Mex.', 'cve_cap'=>'1060001', 'nom_cap'=>'Toluca de Lerdo', 'ptot'=>'15175862', 'pmas'=>'7396986', 'pfem'=>'7778876', 'vtot'=>'3749499']); 
        \DB::table('core.testado')->insert(['cve_ent'=>'16' ,'nom_ent'=>'Michoacán de Ocampo','nom_abr'=>'Mich.', 'cve_cap'=>'0530001', 'nom_cap'=>'Morelia', 'ptot'=>'4351037', 'pmas'=>'2102109', 'pfem'=>'2248928', 'vtot'=>'1082772']); 
        \DB::table('core.testado')->insert(['cve_ent'=>'17' ,'nom_ent'=>'Morelos','nom_abr'=>'Mor.', 'cve_cap'=>'0070001', 'nom_cap'=>'Cuernavaca', 'ptot'=>'1777227', 'pmas'=>'858588', 'pfem'=>'918639', 'vtot'=>'469091']); 
        \DB::table('core.testado')->insert(['cve_ent'=>'18' ,'nom_ent'=>'Nayarit','nom_abr'=>'Nay.', 'cve_cap'=>'0170001', 'nom_cap'=>'Tepic', 'ptot'=>'1084979', 'pmas'=>'541007', 'pfem'=>'543972', 'vtot'=>'291163']); 
        \DB::table('core.testado')->insert(['cve_ent'=>'19' ,'nom_ent'=>'Nuevo León','nom_abr'=>'NL', 'cve_cap'=>'0390001', 'nom_cap'=>'Monterrey', 'ptot'=>'4653458', 'pmas'=>'2320185', 'pfem'=>'2333273', 'vtot'=>'1210893']); 
        \DB::table('core.testado')->insert(['cve_ent'=>'20' ,'nom_ent'=>'Oaxaca','nom_abr'=>'Oax.', 'cve_cap'=>'0670001', 'nom_cap'=>'Oaxaca de Juárez', 'ptot'=>'3801962', 'pmas'=>'1819008', 'pfem'=>'1982954', 'vtot'=>'941814']); 
        \DB::table('core.testado')->insert(['cve_ent'=>'21' ,'nom_ent'=>'Puebla','nom_abr'=>'Pue.', 'cve_cap'=>'1140001', 'nom_cap'=>'Heróica Puebla de Zaragoza', 'ptot'=>'5779829', 'pmas'=>'2769855', 'pfem'=>'3009974', 'vtot'=>'1392053']); 
        \DB::table('core.testado')->insert(['cve_ent'=>'22' ,'nom_ent'=>'Querétaro','nom_abr'=>'Qro.', 'cve_cap'=>'0140001', 'nom_cap'=>'Santiago de Querétaro', 'ptot'=>'1827937', 'pmas'=>'887188', 'pfem'=>'940749', 'vtot'=>'454392']); 
        \DB::table('core.testado')->insert(['cve_ent'=>'23' ,'nom_ent'=>'Quintana Roo','nom_abr'=>'Q. Roo', 'cve_cap'=>'0040001', 'nom_cap'=>'Chetumal', 'ptot'=>'1325578', 'pmas'=>'673220', 'pfem'=>'652358', 'vtot'=>'369326']); 
        \DB::table('core.testado')->insert(['cve_ent'=>'24' ,'nom_ent'=>'San Luis Potosí','nom_abr'=>'SLP', 'cve_cap'=>'0280001', 'nom_cap'=>'San Luis Potosí', 'ptot'=>'2585518', 'pmas'=>'1260366', 'pfem'=>'1325152', 'vtot'=>'639265']); 
        \DB::table('core.testado')->insert(['cve_ent'=>'25' ,'nom_ent'=>'Sinaloa','nom_abr'=>'Sin.', 'cve_cap'=>'0060001', 'nom_cap'=>'Culiacán Rosales', 'ptot'=>'2767761', 'pmas'=>'1376201', 'pfem'=>'1391560', 'vtot'=>'713296']); 
        \DB::table('core.testado')->insert(['cve_ent'=>'26' ,'nom_ent'=>'Sonora','nom_abr'=>'Son.', 'cve_cap'=>'0300001', 'nom_cap'=>'Hermosillo', 'ptot'=>'2662480', 'pmas'=>'1339612', 'pfem'=>'1322868', 'vtot'=>'712402']); 
        \DB::table('core.testado')->insert(['cve_ent'=>'27' ,'nom_ent'=>'Tabasco','nom_abr'=>'Tab.', 'cve_cap'=>'0040001', 'nom_cap'=>'Villahermosa', 'ptot'=>'2238603', 'pmas'=>'1100758', 'pfem'=>'1137845', 'vtot'=>'567233']); 
        \DB::table('core.testado')->insert(['cve_ent'=>'28' ,'nom_ent'=>'Tamaulipas','nom_abr'=>'Tamps.', 'cve_cap'=>'0410001', 'nom_cap'=>'Ciudad Victoria', 'ptot'=>'3268554', 'pmas'=>'1616201', 'pfem'=>'1652353', 'vtot'=>'901387']); 
        \DB::table('core.testado')->insert(['cve_ent'=>'29' ,'nom_ent'=>'Tlaxcala','nom_abr'=>'Tlax.', 'cve_cap'=>'0330001', 'nom_cap'=>'Tlaxcala de Xicohténcatl', 'ptot'=>'1169936', 'pmas'=>'565775', 'pfem'=>'604161', 'vtot'=>'274243']); 
        \DB::table('core.testado')->insert(['cve_ent'=>'30' ,'nom_ent'=>'Veracruz de Ignacio de la Llave','nom_abr'=>'Ver.', 'cve_cap'=>'0870001', 'nom_cap'=>'Xalapa-Enríquez', 'ptot'=>'7643194', 'pmas'=>'3695679', 'pfem'=>'3947515', 'vtot'=>'2014588']); 
        \DB::table('core.testado')->insert(['cve_ent'=>'31' ,'nom_ent'=>'Yucatán','nom_abr'=>'Yuc.', 'cve_cap'=>'0500001', 'nom_cap'=>'Mérida', 'ptot'=>'1955577', 'pmas'=>'963333', 'pfem'=>'992244', 'vtot'=>'507248']); 
        \DB::table('core.testado')->insert(['cve_ent'=>'32' ,'nom_ent'=>'Zacatecas','nom_abr'=>'Zac.', 'cve_cap'=>'0560001', 'nom_cap'=>'Zacatecas', 'ptot'=>'1490668', 'pmas'=>'726897', 'pfem'=>'763771', 'vtot'=>'377018']); 
        \DB::table('core.testado')->update(['estado_id'=>\DB::raw('cast(cve_ent as int)')]);
    }
}
