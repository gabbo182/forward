<?php

use Illuminate\Database\Seeder;

class core_tcuenta extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('core.tcuenta')->insert([
            'persona_id'=> 1,
            'rol_id'=> 1,
            'usuario'=>'Admin',
            'password'=>bcrypt('123456'),
            'email'=>'gabriieldamian.182@gmail.com',
            'estatus'=> 1
        ]);
        
        \DB::table('core.tcuenta')->insert([
            'persona_id'=> 2,
            'rol_id'=> 2,
            'usuario'=>'Joel',
            'password'=>bcrypt('123456'),
            'email'=>'da280561@uahe.edu.mx',
            'estatus'=> 1
        ]);
        
        \DB::table('core.tcuenta')->insert([
            'persona_id'=> 3,
            'rol_id'=> 3,
            'usuario'=>'Gabriel',
            'password'=>bcrypt('123456'),
            'email'=>'gabo_punk-e182@hotmail.com',
            'estatus'=> 1
        ]);
    }
}
