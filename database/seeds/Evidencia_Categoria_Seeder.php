<?php

use Illuminate\Database\Seeder;

class Evidencia_Categoria_Seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('evidencias.categoria')->insert([
            'nombre' => 'Opcional'
        ]);
        \DB::table('evidencias.categoria')->insert([
            'nombre' => 'Obligatorio'
        ]);
    }
}
