<?php

use Illuminate\Database\Seeder;

class core_tpersona extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('core.tpersona')->insert([
            'nombre'=>'Administrador General',
            'apaterno'=>'',
            'amaterno'=>'',
            'curp'=>'DAMJ920525HHGMRL01',
            'telefono'=>'771526454',
            'tel_celular'=>'771526454',
            'fecha_nacimiento'=>'11/11/1973',
            'email'=>'operations@madala-academy.mx',
            'sexo'=>2,
            'estado_id'=>1,
            'municipio_id'=>1,
            'localidad_id'=>1
        ]);
        
        \DB::table('core.tpersona')->insert([
            'nombre'=>'Joel',
            'apaterno'=>'Damián',
            'amaterno'=>'',
            'curp'=>'DAMJ920525HHGMRL01',
            'telefono'=>'771526454',
            'tel_celular'=>'771526454',
            'fecha_nacimiento'=>'11/11/1973',
            'email'=>'',
            'sexo'=>2,
            'estado_id'=>1,
            'municipio_id'=>1,
            'localidad_id'=>1
        ]);
        
        \DB::table('core.tpersona')->insert([
            'nombre'=>'Gabriel',
            'apaterno'=>'Martínez',
            'amaterno'=>'',
            'curp'=>'DAMJ920525HHGMRL01',
            'telefono'=>'771526454',
            'tel_celular'=>'771526454',
            'fecha_nacimiento'=>'11/11/1973',
            'email'=>'',
            'sexo'=>2,
            'estado_id'=>1,
            'municipio_id'=>1,
            'localidad_id'=>1
        ]);
    }
}
