<?php

use Illuminate\Database\Seeder;

class localidad_prueba extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('core.tlocalidad')->insert(['estado_id'=>1 ,'municipio_id'=>1 ,'cve_ent'=>'01' ,'nom_ent'=>'Aguascalientes','nom_abr'=>'Ags.' ,'cve_mun'=>'001','nom_mu'=>'Aguascalientes','cve_loc'=>' ', 'localidad_id'=>1,'nom_loc'=>'Aguascalientes','latitud'=>'215247.362','longitud'=>'1021745.768','altitud'=>'1878','cve_carta'=>'F13D19','ambito'=>'U','ptot'=>'722250','pmas'=>'348722','pfem'=>'373528','vtot'=>'185120']);
    }
}
