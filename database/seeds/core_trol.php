<?php

use Illuminate\Database\Seeder;

class core_trol extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('core.trol')->insert([
            'nombre' => 'Adminastrador',
            'descripcion'=>'Administrador del sistema',
            'rutainicial'=>'/Sistema/inicio',
            'estatus' => 1
        ]);
        
        \DB::table('core.trol')->insert([
            'nombre' => 'Revisor',
            'descripcion'=>'Personal encargado de revisar las evidencias fotográficas',
            'rutainicial'=>'/Sistema/inicio',
            'estatus' => 1
        ]);
        
        \DB::table('core.trol')->insert([
            'nombre' => 'El que sube',
            'descripcion'=>'Personal encargado de subir sus evidencias fotográficas',
            'rutainicial'=>'/Sistema/inicio',
            'estatus' => 1
        ]);
    }
}
